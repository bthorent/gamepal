package com.benthofactory.gamepal.ui.newgame;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class NewGameHandlingPagerAdapter extends FragmentStatePagerAdapter {

    public NewGameHandlingPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new NewGameFragment();
            default:
                return new AllGamesFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "New game";
            default:
                return "Played games";
        }
    }
}
