package com.benthofactory.gamepal.ui.newgame;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.Game;
import com.benthofactory.gamepal.model.PlayerScore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class GamesRecyclerViewAdapter extends RecyclerView.Adapter<GamesRecyclerViewAdapter.ViewHolder> implements Filterable {

    private ArrayList<Game> mGames;
    private AllGamesViewModel viewModel;
    private Context context;
    private FragmentActivity activity;
    private View view;
    private GamesFilter filter;
    private int filteringMode = GamesFilter.FILTER_GAMES;

    public GamesRecyclerViewAdapter(Context context, FragmentActivity activity) {
        this.context = context;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_games, parent, false);
        return new ViewHolder(view);
    }

    public void setFilteringMode(int filteringMode) {
        this.filteringMode = filteringMode;
    }

    public int getFilteringMode() {
        return this.filteringMode;
    }

    public void setViewModel(AllGamesViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public void setGamesList(final ArrayList<Game> update) {
        Collections.sort(update, (lg, rg) -> rg.getDate().compareTo(lg.getDate()));
        if (mGames == null) {
            mGames = update;
            notifyItemRangeInserted(0, update.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mGames.size();
                }

                @Override
                public int getNewListSize() {
                    return update.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return mGames.get(oldItemPosition).getId() == update.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Game newGame = update.get(newItemPosition);
                    Game oldGame = mGames.get(oldItemPosition);
                    return newGame.getId() == oldGame.getId()
                            && Objects.equals(newGame.getName(), oldGame.getName());
                }
            });
            mGames = update;
            result.dispatchUpdatesTo(this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Game game = mGames.get(position);
        holder.mItem = game;

        holder.mGameNameText.setText(game.getName());

        holder.mMainBody.setOnClickListener(v -> {
            //EditGameActivity.start(activity, game);
            Log.i("BEN_TEST", game.toString());
        });

        String playersText = "No player";
        List<PlayerScore> scoresForGame = viewModel.getPlayerscoresForGame(game);
        if (scoresForGame != null) {
            playersText = "Players (" + scoresForGame.size() + ") : ";
            for (PlayerScore p : scoresForGame) {
                playersText += p.getName() + ", ";
            }
            playersText = playersText.substring(0, playersText.length() - 2);
        }
        holder.mGamePlayersText.setText(playersText);

        holder.mGameDateText.setText(context.getResources().getString(R.string.beginning_date) + " " + game.getDateForDisplay());
    }

    @Override
    public int getItemCount() {
        return (mGames != null ? mGames.size() : 0);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new GamesFilter(mGames,this, filteringMode);
        }
        return filter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final View mMainBody;
        public final TextView mGameNameText;
        public final TextView mGamePlayersText;
        public final TextView mGameDateText;
        public Game mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mMainBody = view.findViewById(R.id.main_body);
            mGameNameText = view.findViewById(R.id.game_name);
            mGamePlayersText = view.findViewById(R.id.game_players);
            mGameDateText = view.findViewById(R.id.game_date);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mGameNameText.getText() + "'";
        }
    }
}
