package com.benthofactory.gamepal.ui.newgame;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.PlayerScore;
import com.benthofactory.gamepal.utils.ColorUtil;
import com.google.android.material.card.MaterialCardView;

import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class PlayerScoresAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemDragAdapter {

    private static final int MODE_INCREASE_VALUE = 1;
    private static final int MODE_DECREASE_VALUE = 2;
    private static final int MODE_SET_VALUE = 3;

    static final String INCREASE_VALUE_CLICK = "increase_value_click";
    static final String DECREASE_VALUE_CLICK = "decrease_value_click";
    private final CounterActionCallback callback;
    private List<PlayerScore> playerScores;
    private PlayerScore lastMovedPlayerScore = null;
    private boolean isEditableMode;

    PlayerScoresAdapter(CounterActionCallback callback, boolean isEditableMode) {
        this.callback = callback;
        this.isEditableMode = isEditableMode;
    }

    @Override
    public int getItemCount() {
        return playerScores == null ? 0 : playerScores.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_counter_full, parent, false);
        return new CounterFullViewHolder(v, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final PlayerScore playerScore = playerScores.get(position);
        int counterColor;
        try {
            counterColor = Color.parseColor(playerScore.getColor());
        } catch (Exception e) {
            counterColor = Color.parseColor("#000000");
        }
        CounterFullViewHolder holder = (CounterFullViewHolder) viewHolder;
        holder.playerScore = playerScore;
        holder.counterName.setText(playerScore.getName());
        holder.counterValue.setText(String.valueOf(playerScore.getScore()));
        holder.container.setCardBackgroundColor(counterColor);
        final boolean darkBackground = ColorUtil.isDarkBackground(counterColor);
        int tintColor = darkBackground ? 0xDEFFFFFF : 0xDE000000;
        holder.counterName.setTextColor(tintColor);
        holder.counterValue.setTextColor(darkBackground ? Color.WHITE : 0xDE000000);
        holder.currentIncrement.setTextColor(darkBackground ? Color.WHITE : 0xDE000000);
        holder.roundsDetails.setTextColor(tintColor);
        holder.roundsDetails.setMovementMethod(new ScrollingMovementMethod());

        Drawable wrapDrawable1 = DrawableCompat.wrap(holder.increaseImageView.getDrawable().mutate());
        Drawable wrapDrawable2 = DrawableCompat.wrap(holder.decreaseImageView.getDrawable().mutate());
        Drawable wrapDrawable3 = DrawableCompat.wrap(holder.counterOptionsImageView.getDrawable().mutate());
        DrawableCompat.setTint(wrapDrawable1, tintColor);
        DrawableCompat.setTint(wrapDrawable2, tintColor);
        DrawableCompat.setTint(wrapDrawable3, tintColor);

        boolean hasRound = playerScore.getRoundsDetails()!=null && !playerScore.getRoundsDetails().isEmpty();
        if (hasRound) {
            holder.currentIncrement.setVisibility(View.VISIBLE);
            holder.roundsDetailsSection.setVisibility(View.VISIBLE);

            holder.currentIncrement.setText("(" + (playerScore.getCurrentRoundIncrement()>=0 ? "+" : "") + playerScore.getCurrentRoundIncrement() + ")");
            holder.counterValue.setText(String.valueOf(playerScore.getScore()));
            holder.roundsDetails.setText(playerScore.getRoundsDetailsAsText());
        } else {
            holder.currentIncrement.setVisibility(View.GONE);
            holder.roundsDetailsSection.setVisibility(View.GONE);
        }
    }

    void setPlayerScoresList(final List<PlayerScore> update) {
        if (playerScores == null) {
            playerScores = update;
            notifyItemRangeInserted(0, update.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return playerScores.size();
                }

                @Override
                public int getNewListSize() {
                    return update.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return playerScores.get(oldItemPosition).getId() == update.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    PlayerScore newPlayerScore = update.get(newItemPosition);
                    PlayerScore oldPlayerScore = playerScores.get(oldItemPosition);
                    return newPlayerScore.getId() == oldPlayerScore.getId()
                            && Objects.equals(newPlayerScore.getName(), oldPlayerScore.getName())
                            && Objects.equals(newPlayerScore.getColor(), oldPlayerScore.getColor())
                            && newPlayerScore.getScore() == oldPlayerScore.getScore();
                    // We don`t want to check position, because during dragging items were moved only within adapter.
                    // Position in database was changed only after the end of dragging event.
                    // Checking for position change in this method, would result into recycler view redundant refresh.
                }
            });
            playerScores = update;
            result.dispatchUpdatesTo(this);
        }
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
    }

    @Override
    public void onItemClear(int fromIndex, int toIndex) {
    }

    public class CounterFullViewHolder extends RecyclerView.ViewHolder implements Callback {

        private static final int MSG_PERFORM_LONGCLICK = 1;

        final ImageView decreaseImageView;
        final ImageView increaseImageView;
        final TextView counterValue;
        final LinearLayout roundsDetailsSection;
        final TextView roundsDetails;
        final TextView currentIncrement;
        final MaterialCardView container;

        private final int TIME_LONG_CLICK = 300;
        private final CounterActionCallback counterActionCallback;
        private final FrameLayout counterClickableArea;
        private final ImageView counterOptionsImageView;
        private final TextView counterName;
        private final Handler handler;
        public PlayerScore playerScore;
        private float lastX;
        private LongClickTimerTask timerTask;

        @SuppressLint("ClickableViewAccessibility")
        CounterFullViewHolder(View v, CounterActionCallback callback) {
            super(v);
            counterActionCallback = callback;
            handler = new Handler(this);
            counterValue = v.findViewById(R.id.tv_counter_value);
            counterName = v.findViewById(R.id.tv_counter_name);
            roundsDetailsSection = v.findViewById(R.id.rounds_details_section);
            roundsDetails = v.findViewById(R.id.rounds_details);
            currentIncrement = v.findViewById(R.id.current_increment);
            counterOptionsImageView = v.findViewById(R.id.iv_counter_edit);
            counterClickableArea = v.findViewById(R.id.counter_interaction_area);
            increaseImageView = v.findViewById(R.id.iv_increase);
            decreaseImageView = v.findViewById(R.id.iv_decrease);
            container = v.findViewById(R.id.card);
            final CounterFullViewHolder holder = this;

            if(!isEditableMode){
                counterOptionsImageView.setVisibility(View.GONE);
                increaseImageView.setVisibility(View.GONE);
                decreaseImageView.setVisibility(View.GONE);
                counterName.setClickable(false);
                counterClickableArea.setClickable(false);
            } else {
                counterName.setOnClickListener(v1 -> counterActionCallback.onNameClick(playerScore));
                counterOptionsImageView.setOnClickListener(v2 -> counterActionCallback.onEditClick(playerScore));

                counterClickableArea.setOnTouchListener((v12, e) -> {
                    switch (e.getActionMasked()) {
                        case MotionEvent.ACTION_DOWN:
                            lastX = e.getX();
                            if (timerTask != null) {
                                timerTask.cancel();
                            }
                            // start counting long click time
                            timerTask = new LongClickTimerTask();
                            Timer timer = new Timer();
                            timer.schedule(timerTask, TIME_LONG_CLICK);
                            break;

                        case MotionEvent.ACTION_UP:
                            long time = e.getEventTime() - e.getDownTime();
                            if (time < TIME_LONG_CLICK) {
                                v12.performClick();
                                updateCounter(e.getX());
                            }
                            cancelLongClickTask();
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                        case MotionEvent.ACTION_CANCEL:
                        case MotionEvent.ACTION_POINTER_UP:
                            cancelLongClickTask();
                            break;
                    }
                    return false;
                });
            }
        }

        @Override
        public boolean handleMessage(final Message msg) {
            if (msg.what == MSG_PERFORM_LONGCLICK) {
                if (lastX != -1) {
                    int interactionAreaWidth = counterClickableArea.getWidth() / 3;
                    if (lastX >= interactionAreaWidth * 2) {
                        counterActionCallback.onLongClick(playerScore, getAdapterPosition(), MODE_INCREASE_VALUE);
                    } else if (lastX <= interactionAreaWidth) {
                        counterActionCallback.onLongClick(playerScore, getAdapterPosition(), MODE_DECREASE_VALUE);
                    } else {
                        counterActionCallback.onLongClick(playerScore, getAdapterPosition(), MODE_SET_VALUE);
                    }
                }
            }
            return false;
        }

        private void cancelLongClickTask() {
            if (timerTask != null) {
                timerTask.setNonExecutable();
                timerTask.cancel();
            }
            lastX = -1;
        }

        private void updateCounter(float x) {
            float width = counterClickableArea.getWidth();
            if (x >= width - width / 3) {
                notifyItemChanged(getAdapterPosition(), INCREASE_VALUE_CLICK);
                counterActionCallback.onSingleClick(playerScore, getAdapterPosition(), MODE_INCREASE_VALUE);
            } else if (x <= width / 3) {
                notifyItemChanged(getAdapterPosition(), DECREASE_VALUE_CLICK);
                counterActionCallback.onSingleClick(playerScore, getAdapterPosition(), MODE_DECREASE_VALUE);
            } else {
                counterActionCallback.onSingleClick(playerScore, getAdapterPosition(), MODE_SET_VALUE);
            }
        }

        private class LongClickTimerTask extends TimerTask {

            private boolean exec;

            LongClickTimerTask() {
                exec = true;
            }

            @Override
            public void run() {
                if (exec) {
                    handler.sendEmptyMessage(MSG_PERFORM_LONGCLICK);
                }
            }

            void setNonExecutable() {
                this.exec = false;
            }

        }

    }
}
