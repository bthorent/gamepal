package com.benthofactory.gamepal.ui.dice;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.dynamicanimation.animation.SpringAnimation;
import androidx.dynamicanimation.animation.SpringForce;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.transition.TransitionManager;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.utils.LocalSettings;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

public class DicesFragment extends Fragment {

    private static final String ARG_CURRENT_DICE_ROLL = "ARG_CURRENT_DICE_ROLL";
    private static final String ARG_PREVIOUS_DICE_ROLL = "ARG_PREVIOUS_DICE_ROLL";

    private float accel;
    private float accelCurrent;
    private float accelLast;
    private int previousRoll;
    private int currentRoll;
    private View contentView;
    private TextView previousRollTextView;
    private TextView diceVariantTextView;
    private TextView previousRollTextViewLabel;
    private TextView diceTextView;
    private SpringForce springForce;
    private DiceViewModel viewModel;
    private ConstraintLayout root;
    private OnDiceFragmentInteractionListener listener;
    private int maxSide;

    public DicesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            currentRoll = getArguments().getInt(ARG_CURRENT_DICE_ROLL);
            previousRoll = getArguments().getInt(ARG_PREVIOUS_DICE_ROLL);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.fragment_dice, container, false);
        previousRollTextView = contentView.findViewById(R.id.previous_dice);
        previousRollTextViewLabel = contentView.findViewById(R.id.tv_previous_roll_label);
        diceTextView = contentView.findViewById(R.id.dice);
        root = contentView.findViewById(R.id.container);

        maxSide = LocalSettings.getDiceMaxSide();

        root.setOnClickListener(v -> viewModel.rollDice());

        diceVariantTextView = contentView.findViewById(R.id.dice_variant);
        diceVariantTextView.setOnClickListener(v -> showBottomSheet());
        diceVariantTextView.setText(getString(R.string.dice_id, maxSide));

        return contentView;
    }

    private void showBottomSheet() {
        if (this.getFragmentManager() != null) {
            DiceBottomSheetFragment bottomSheet = new DiceBottomSheetFragment();
            bottomSheet.show(this.getFragmentManager(), "DiceBottomSheetFragment");
            bottomSheet.setOnDismissListener(d -> updateOnDismiss());
        } else {
            Snackbar.make(root, "Unexpected case cannot open bottom sheet", Snackbar.LENGTH_LONG).show();
        }
    }

    private void updateOnDismiss() {
        maxSide = LocalSettings.getDiceMaxSide();
        diceVariantTextView.setText(getString(R.string.dice_id, maxSide));
        viewModel.setDiceVariant(maxSide);
        if (viewModel.getSensorLiveData(getActivity()) == null) {
            initSensorData();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        springForce = new SpringForce()
                .setDampingRatio(SpringForce.DAMPING_RATIO_MEDIUM_BOUNCY)
                .setStiffness(SpringForce.STIFFNESS_VERY_LOW)
                .setFinalPosition(1);
        subscribeUI();
        initSensorData();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnDiceFragmentInteractionListener) {
            listener = (OnDiceFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnDiceFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // No keyboard here if present before
        hideKeyboardFrom(getContext(), contentView);
    }

    private void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void initSensorData() {
        accel = 0.00f;
        accelCurrent = SensorManager.GRAVITY_EARTH;
        accelLast = SensorManager.GRAVITY_EARTH;
        viewModel.getSensorLiveData(getActivity()).observe(getViewLifecycleOwner(), se -> {
            if (se == null) {
                return;
            }

            float x = se.values[0];
            float y = se.values[1];
            float z = se.values[2];
            accelLast = accelCurrent;
            accelCurrent = (float) Math.sqrt(x * x + y * y + z * z);
            float delta = accelCurrent - accelLast;
            accel = accel * 0.9f + delta; // perform low-cut filter
            if (accel > 5.0) {
                viewModel.rollDice();
            }
        });
    }

    private void rollDice(@IntRange(from = 1, to = 100) int roll) {
        updateLastRollLabel();
        previousRoll = roll;
        currentRoll = roll;
        listener.updateCurrentRoll(currentRoll);
        new SpringAnimation(diceTextView, DynamicAnimation.ROTATION)
                .setSpring(springForce)
                .setStartValue(1000f)
                .setStartVelocity(1000)
                .start();
        new SpringAnimation(diceTextView, DynamicAnimation.SCALE_X)
                .setStartValue(0.5f)
                .setSpring(springForce)
                .start();
        new SpringAnimation(diceTextView, DynamicAnimation.ALPHA)
                .setStartValue(0.1f)
                .setSpring(springForce)
                .start();
        new SpringAnimation(diceTextView, DynamicAnimation.SCALE_Y)
                .setStartValue(0.5f)
                .setSpring(springForce)
                .start();

        diceTextView.setText(String.valueOf(roll));
    }

    @SuppressLint("SetTextI18n")
    private void updateLastRollLabel() {
        TransitionManager.beginDelayedTransition(root);
        if (previousRoll != 0) {
            previousRollTextViewLabel.setVisibility(View.VISIBLE);
            previousRollTextView.setVisibility(View.VISIBLE);
            previousRollTextView.setText("" + previousRoll);
            ObjectAnimator scaleArrowAnimator =
                    ObjectAnimator.ofPropertyValuesHolder(previousRollTextView,
                            PropertyValuesHolder.ofFloat("scaleX", 1.0f, 1.54f, 1.0f),
                            PropertyValuesHolder.ofFloat("scaleY", 1.0f, 1.54f, 1.0f));
            scaleArrowAnimator.start();
        }
    }

    private void subscribeUI() {
        DiceViewModelFactory factory = new DiceViewModelFactory(LocalSettings.getDiceMaxSide());
        viewModel = new ViewModelProvider(this, factory).get(DiceViewModel.class);
        final DiceLiveData diceLiveData = viewModel.getDiceLiveData();
        diceLiveData.observe(getViewLifecycleOwner(), roll -> {
            if (roll != null && roll > 0) {
                rollDice(roll);
            } else if (currentRoll > 0) {
                updateLastRollLabel();
                diceTextView.setText(String.valueOf(currentRoll));
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

}
