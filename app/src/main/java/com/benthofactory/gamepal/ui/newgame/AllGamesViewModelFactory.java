package com.benthofactory.gamepal.ui.newgame;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class AllGamesViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final Application app;

    public AllGamesViewModelFactory(Application gamePalApp) {
        app = gamePalApp;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        //noinspection unchecked
        return (T) new AllGamesViewModel(app);
    }
}
