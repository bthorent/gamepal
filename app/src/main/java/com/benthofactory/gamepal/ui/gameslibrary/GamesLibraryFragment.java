package com.benthofactory.gamepal.ui.gameslibrary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.LibraryGame;
import com.benthofactory.gamepal.ui.editgamelibrary.EditLibraryGameActivity;
import com.benthofactory.gamepal.utils.LocalSettings;

import java.util.ArrayList;
import java.util.Arrays;

import static com.benthofactory.gamepal.ui.gameslibrary.LibraryGamesFilter.CONSTRAINT_MAX_DURATION;
import static com.benthofactory.gamepal.ui.gameslibrary.LibraryGamesFilter.CONSTRAINT_MAX_NBR_OF_PLAYERS;
import static com.benthofactory.gamepal.ui.gameslibrary.LibraryGamesFilter.CONSTRAINT_MIN_DURATION;
import static com.benthofactory.gamepal.ui.gameslibrary.LibraryGamesFilter.CONSTRAINT_MIN_NBR_OF_PLAYERS;
import static com.benthofactory.gamepal.ui.gameslibrary.LibraryGamesFilter.CONSTRAINT_NAME;

public class GamesLibraryFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private LibraryGameRecyclerViewAdapter gamesListAdapter;

    private TextView noActiveFilterTextView;
    private TextView nameFilterText;
    private TextView playersFilterText;
    private TextView durationFilterText;
    private View clearFiltersBtn;
    private  TextView sortingTypeText;

    private View filterActiveSection;
    private View sortingDetailsSection;


    public GamesLibraryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFilters();
    }

    public void initFilters() {
        LocalSettings.saveGameNamePatternFilter("");
        LocalSettings.saveMinPlayerFilter(-1);
        LocalSettings.saveMaxPlayerFilter(-1);
        LocalSettings.saveMinDurationFilter(-1);
        LocalSettings.saveMaxDurationFilter(-1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_librarygame_list, container, false);

        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        filterActiveSection = view.findViewById(R.id.active_filter_section);
        filterActiveSection.setOnClickListener(v -> showBottomSheet());

        noActiveFilterTextView = view.findViewById(R.id.no_active_filter_text);
        nameFilterText = view.findViewById(R.id.name_filter_text);
        playersFilterText = view.findViewById(R.id.players_filter_text);
        durationFilterText = view.findViewById(R.id.duration_filter_text);
        clearFiltersBtn = view.findViewById(R.id.cancel_filter_btn);

        sortingDetailsSection = view.findViewById(R.id.active_sorting_section);
        sortingDetailsSection.setOnClickListener(v -> showSortingPreferenceDialog());
        sortingTypeText = view.findViewById(R.id.sorting_type_text);

        view.findViewById(R.id.cancel_filter_btn).setOnClickListener(v -> {
            initFilters();
            updateActiveFilterSectionDisplay();
            setupFilter();
        });

        updateActiveFilterSectionDisplay();

        view.findViewById(R.id.add_game_btn).setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), EditLibraryGameActivity.class);
            startActivity(intent);
        });

        ArrayList<LibraryGame> games = new ArrayList<>(DatabaseHolder.database().gamesDao().getAllGamesInLibrary());
        gamesListAdapter = new LibraryGameRecyclerViewAdapter(games, mListener);
        recyclerView.setAdapter(gamesListAdapter);

        return view;
    }

    private void showSortingPreferenceDialog() {
        final String[] items = {"Alphabetical order", "By category", "By Rank", "By times played"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select sorting preference");
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
        int checkedItem = Arrays.asList(items).indexOf(sortingTypeText.getText().toString());
        builder.setSingleChoiceItems(
                items,
                checkedItem,
                (dialogInterface, i) -> {
                    sortingTypeText.setText(Arrays.asList(items).get(i));
                    switch (i) {
                        case 0:
                            gamesListAdapter.setOrderingMode(LibraryGameRecyclerViewAdapter.OrderingMode.ALPHABETICAL_ORDER);
                            break;
                        case 1:
                            gamesListAdapter.setOrderingMode(LibraryGameRecyclerViewAdapter.OrderingMode.CATEGORY_ORDER);
                            break;
                        case 2:
                            gamesListAdapter.setOrderingMode(LibraryGameRecyclerViewAdapter.OrderingMode.RANK_ORDER);
                            break;
                        case 3:
                            gamesListAdapter.setOrderingMode(LibraryGameRecyclerViewAdapter.OrderingMode.TIMES_PLAYED_ORDER);
                            break;
                    }
                    gamesListAdapter.notifyDataSetChanged();
                    recyclerView.smoothScrollToPosition(0);
                    dialogInterface.dismiss();
                });
        builder.show();
    }

    private void setupFilter() {
        ((LibraryGamesFilter)gamesListAdapter.getFilter()).resetConstraints();
        ((LibraryGamesFilter)gamesListAdapter.getFilter()).addConstraint(CONSTRAINT_NAME, LocalSettings.getGameNamePatternFilter());
        if (LocalSettings.getMinPlayerFilter()!=-1) {
            ((LibraryGamesFilter)gamesListAdapter.getFilter()).addConstraint(CONSTRAINT_MIN_NBR_OF_PLAYERS, String.valueOf(LocalSettings.getMinPlayerFilter()));
        }
        if (LocalSettings.getMaxPlayerFilter()!=-1) {
            ((LibraryGamesFilter)gamesListAdapter.getFilter()).addConstraint(CONSTRAINT_MAX_NBR_OF_PLAYERS, String.valueOf(LocalSettings.getMaxPlayerFilter()));
        }
        if (LocalSettings.getMinDurationFilter()!=-1) {
            ((LibraryGamesFilter)gamesListAdapter.getFilter()).addConstraint(CONSTRAINT_MIN_DURATION, String.valueOf(LocalSettings.getMinDurationFilter()));
        }
        if (LocalSettings.getMaxDurationFilter()!=-1) {
            ((LibraryGamesFilter)gamesListAdapter.getFilter()).addConstraint(CONSTRAINT_MAX_DURATION, String.valueOf(LocalSettings.getMaxDurationFilter()));
        }
        gamesListAdapter.getFilter().filter("");
    }

    private void showBottomSheet() {
        if (getActivity().getSupportFragmentManager() != null) {
            FilterLibraryBottomSheetFragment bottomSheet = new FilterLibraryBottomSheetFragment();
            bottomSheet.show(getActivity().getSupportFragmentManager(), "FilterLibraryBottomSheetFragment");
            bottomSheet.setOnDismissListener(d -> updateOnDismiss());
        }
    }

    private void updateActiveFilterSectionDisplay() {
        boolean hasActiveFilter = false;

        if(!LocalSettings.getGameNamePatternFilter().equals("")) {
            nameFilterText.setVisibility(View.VISIBLE);
            hasActiveFilter = true;
            nameFilterText.setText("Name pattern [" + LocalSettings.getGameNamePatternFilter() + "]");
        } else {
            nameFilterText.setVisibility(View.GONE);
        }

        if(LocalSettings.getMinPlayerFilter()!=-1) {
            playersFilterText.setVisibility(View.VISIBLE);
            hasActiveFilter = true;
            if(LocalSettings.getMaxPlayerFilter()!=-1) {
                playersFilterText.setText("Between " + LocalSettings.getMinPlayerFilter() + " and " + LocalSettings.getMaxPlayerFilter() + " player(s)");
            } else {
                playersFilterText.setText("More than " + LocalSettings.getMinPlayerFilter() + " player(s)");
            }
        } else if(LocalSettings.getMaxPlayerFilter()!=-1) {
            playersFilterText.setVisibility(View.VISIBLE);
            hasActiveFilter = true;
            playersFilterText.setText("Less than " + LocalSettings.getMaxPlayerFilter() + " player(s)");
        } else {
            playersFilterText.setVisibility(View.GONE);
        }

        if(LocalSettings.getMinDurationFilter()!=-1) {
            durationFilterText.setVisibility(View.VISIBLE);
            hasActiveFilter = true;
            if(LocalSettings.getMaxDurationFilter()!=-1) {
                durationFilterText.setText("Between " + LocalSettings.getMinDurationFilter() + " and " + LocalSettings.getMaxDurationFilter() + " min(s)");
            } else {
                durationFilterText.setText("More than " + LocalSettings.getMinDurationFilter() + " min(s)");
            }
        } else if(LocalSettings.getMaxDurationFilter()!=-1) {
            durationFilterText.setVisibility(View.VISIBLE);
            hasActiveFilter = true;
            durationFilterText.setText("Less than " + LocalSettings.getMaxDurationFilter() + " min(s)");
        } else {
            durationFilterText.setVisibility(View.GONE);
        }

        clearFiltersBtn.setVisibility((hasActiveFilter ? View.VISIBLE : View.GONE));
        noActiveFilterTextView.setVisibility((hasActiveFilter ? View.GONE : View.VISIBLE));
    }

    private void updateOnDismiss() {
        updateActiveFilterSectionDisplay();
        setupFilter();
    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<LibraryGame> games = new ArrayList<>(DatabaseHolder.database().gamesDao().getAllGamesInLibrary());
        gamesListAdapter = new LibraryGameRecyclerViewAdapter(games, mListener);
        recyclerView.setAdapter(gamesListAdapter);
        updateActiveFilterSectionDisplay();
        setupFilter();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(LibraryGame item);
    }

}
