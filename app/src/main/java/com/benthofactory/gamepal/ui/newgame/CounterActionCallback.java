package com.benthofactory.gamepal.ui.newgame;

import com.benthofactory.gamepal.model.PlayerScore;

public interface CounterActionCallback {

    void onSingleClick(PlayerScore playerScore, int position, int mode);

    void onLongClick(PlayerScore playerScore, int position, int mode);

    void onNameClick(PlayerScore playerScore);

    void onEditClick(PlayerScore playerScore);

}
