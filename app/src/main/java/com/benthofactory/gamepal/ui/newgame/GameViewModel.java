package com.benthofactory.gamepal.ui.newgame;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.DatabaseHelper;
import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.Game;
import com.benthofactory.gamepal.model.GamesDao;
import com.benthofactory.gamepal.model.PlayerScore;
import com.benthofactory.gamepal.utils.LocalSettings;
import com.benthofactory.gamepal.utils.SnackbarMessage;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

class GameViewModel extends AndroidViewModel {

    private final GamesDao gamesDao;
    private final String[] colors;
    private Context context;
    private SnackbarMessage snackbarMessage = new SnackbarMessage();
    private LiveData<List<PlayerScore>> playerScores;

    GameViewModel(Application application, Context contextt) {
        super(application);
        this.context = context;

        gamesDao = DatabaseHolder.database().gamesDao();
        if (getDraftGame() != null) {
            playerScores = gamesDao.getPlayerScoresForGameAsLiveData(getDraftGame().getId());
        }

        String[] arr = application.getResources().getStringArray(R.array.colors);
        shuffleColors(arr);
        colors = arr;
    }

    public Game getDraftGame() {
        for (Game g : DatabaseHolder.database().gamesDao().getAllGames()) {
            if (g.isDraftGame()){
                return g;
            }
        }
        return null;
    }

    void cancelGameCreation() {
        DatabaseHolder.database().gamesDao().deleteGame(getDraftGame());
    }

    public void createGame() {
        Game newGame = new Game();
        newGame.setDate(new Date(System.currentTimeMillis()));
        String playerScoreName = "";
        if (LocalSettings.hasDefaultPlayerCreationForNewGamesActivated()) {
            playerScoreName = (LocalSettings.getNameInApp()!=null ? LocalSettings.getNameInApp() : "");
        }
        (new DatabaseHelper(DatabaseHolder.database())).saveGame(newGame);
        addPlayerScore(playerScoreName, false);
    }

    LiveData<List<PlayerScore>> getPlayerScores() {
        return playerScores;
    }

    void updateGameName(@NonNull String name) {
        Game gameOnEdit = getDraftGame();
        if (gameOnEdit!=null) {
            gameOnEdit.setName(name);
            (new DatabaseHelper(DatabaseHolder.database())).saveGame(gameOnEdit);
        }
    }

    void saveGame() {
        Game gameOnEdit = getDraftGame();
        gameOnEdit.setStatus(Game.GAME_STATUS_ACTIVE);
        gameOnEdit.setDate(new Date(System.currentTimeMillis()));
        (new DatabaseHelper(DatabaseHolder.database())).saveGame(gameOnEdit);
    }

    void addPlayerScore() {
        addPlayerScore("", true);
    }

    void addPlayerScore(String counterName, boolean displaySnackbar) {
        int size = 1;
        if (playerScores != null && playerScores.getValue() != null){
            size = playerScores.getValue().size() + 1;
        }

        Log.i("BEN_TEST", DatabaseHolder.database().gamesDao().getAllGames().toString());


        List<PlayerScore> existingPlayerScores = DatabaseHolder.database().gamesDao().getPlayerScoresForGame(getDraftGame().getId());
        int numberOfRounds = 0;
        if (existingPlayerScores!=null && !existingPlayerScores.isEmpty()){
            numberOfRounds = existingPlayerScores.get(0).getNumberOfRounds();
        }

        String counterNameToUse =  counterName.isEmpty() ? getApplication().getString(R.string.player) + " #" + size : counterName;
        (new DatabaseHelper(DatabaseHolder.database())).createPlayerScore(getDraftGame().getId(), counterNameToUse, getNextColor(size), size, numberOfRounds)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                        if (displaySnackbar) {
                            showSnackbarMessage(R.string.counter_added);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "create playerScore");
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    public void moveToNextRound() {
        for (PlayerScore ps : DatabaseHolder.database().gamesDao().getPlayerScoresForGame(getDraftGame().getId())) {
            (new DatabaseHelper(DatabaseHolder.database())).addRoundToPlayerScore(ps)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new CompletableObserver() {
                        @Override
                        public void onComplete() {
                            showSnackbarMessage(R.string.counter_added);
                        }
                        @Override
                        public void onError(Throwable e) {
                            Timber.e(e, "update playerScore");
                        }
                        @Override
                        public void onSubscribe(Disposable d) {}
                    });
        }
    }

    void decreaseCounter(PlayerScore playerScore, @IntRange(from = Integer.MIN_VALUE, to = 0) int amount) {
        (new DatabaseHelper(DatabaseHolder.database())).modifyScore(playerScore.getId(), amount)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "modifyCount playerScore");
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void increaseCounter(PlayerScore playerScore, @IntRange(from = 0, to = Integer.MAX_VALUE) int amount) {
        (new DatabaseHelper(DatabaseHolder.database())).modifyScore(playerScore.getId(), amount)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "modifyCount playerScore");
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void modifyName(PlayerScore playerScore, @NonNull String name) {
        (new DatabaseHelper(DatabaseHolder.database())).modifyName(playerScore.getId(), name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "modifyName playerScore");
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void resetCounter(PlayerScore playerScore) {
        (new DatabaseHelper(DatabaseHolder.database())).setScore(playerScore.getId(), 0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void modifyCurrentValue(PlayerScore playerScore, @IntRange(from = 0, to = Integer.MAX_VALUE) int newValue) {
        if (newValue == playerScore.getScore()) {
            return;
        }

        (new DatabaseHelper(DatabaseHolder.database())).setScore(playerScore.getId(), newValue)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    public LiveData<PlayerScore> getCounterLiveData(long playerScoreId) {
        return (new DatabaseHelper(DatabaseHolder.database())).getPlayerScoresAsLiveData(playerScoreId);
    }

    /*public Game getGameOnEdit() {
        Game result = null;
        if (gameOnEdit != null) {
            result = repository.getGame(gameOnEdit.getId());
        }
        return result;
    }

    public String getGameUnderCreationName() {
        String result = "";
        if (gameOnEdit != null) {
            result = getGameOnEdit().getName();
        }
        return result;
    }

    public LiveData<PlayerScore> getCounterLiveData(int counterID) {
        return repository.loadCounter(counterID);
    }

    void saveGame() {
        gameOnEdit.setStatus(Game.GAME_STATUS_ACTIVE);
        gameOnEdit.setDate(new Date(System.currentTimeMillis()));
        repository.saveGame(gameOnEdit);
        gameOnEdit = null;
    }

    void cancelGameCreation() {
        repository.deleteGame(gameOnEdit);
        gameOnEdit = null;
    }

    public void createGame() {
        repository.saveGame(Game.crateDraftGame());
        gameOnEdit = repository.getDraftGame();
        if (LocalSettings.hasDefaultPlayerCreationForNewGamesActivated()) {
            addPlayerScore((LocalSettings.getNameInApp()!=null ? LocalSettings.getNameInApp() : getApplication().getString(R.string.player) + " #1"), false);
        }
        playerScores = repository.getPlayerScoresForGameAsLiveData(gameOnEdit.getId());
    }

    public void moveToNextRound() {
        for (PlayerScore ps : DatabaseHolder.database().gamesDao().getPlayerScoresForGame(gameOnEdit.getId())) {
            repository.addRoundToPlayerScore(ps)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                        showSnackbarMessage(R.string.counter_added);
                    }
                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "update playerScore");
                    }
                    @Override
                    public void onSubscribe(Disposable d) {}
                });
        }
    }

    void addPlayerScore() {
        addPlayerScore("", true);
    }

    void addPlayerScore(String counterName, boolean displaySnackbar) {
        int size = 1;
        if (playerScores != null && playerScores.getValue() != null){
            size = playerScores.getValue().size() + 1;
        }
        List<PlayerScore> existingPlayerScores = DatabaseHolder.database().gamesDao().getPlayerScoresForGame(gameOnEdit.getId());
        int numberOfRounds = 0;
        if (existingPlayerScores!=null && !existingPlayerScores.isEmpty()){
            Log.i("BEN_TEST", existingPlayerScores.get(0).getRoundsDetails());
            numberOfRounds = existingPlayerScores.get(0).getRoundsDetails().split(";").length;
        }

        String counterNameToUse =  counterName.isEmpty() ? getApplication().getString(R.string.player) + " #" + size : counterName;
        repository.createCounter(gameOnEdit.getId(), counterNameToUse, getNextColor(size), size, numberOfRounds)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                        if (displaySnackbar) {
                            showSnackbarMessage(R.string.counter_added);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "create playerScore");
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    LiveData<List<PlayerScore>> getPlayerScores() {
        return playerScores;
    }

    void updateGameName(@NonNull String name) {
        gameOnEdit.setName(name);
        repository.saveGame(gameOnEdit);
    }

    void updateGameDate(@NonNull Date date) {
        gameOnEdit.setDate(date);
        repository.saveGame(gameOnEdit);
    }

    void decreaseCounter(PlayerScore playerScore, @IntRange(from = Integer.MIN_VALUE, to = 0) int amount) {
        repository.modifyCount(playerScore.getId(), amount)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "modifyCount playerScore");
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void increaseCounter(PlayerScore playerScore, @IntRange(from = 0, to = Integer.MAX_VALUE) int amount) {
        repository.modifyCount(playerScore.getId(), amount)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "modifyCount playerScore");
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void modifyName(PlayerScore playerScore, @NonNull String name) {
        repository.modifyName(playerScore.getId(), name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "modifyName playerScore");
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void modifyCurrentValue(PlayerScore playerScore, @IntRange(from = 0, to = Integer.MAX_VALUE) int newValue) {
        if (newValue == playerScore.getValue()) {
            return;
        }

        repository.setCount(playerScore.getId(), newValue)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void modifyPosition(PlayerScore playerScore, int fromIndex, int toIndex) {

        if (fromIndex == toIndex) return;

        List<PlayerScore> playerScoreList = playerScores.getValue();
        if (playerScoreList == null) return;

        // PlayerScore's position starts from 1, not from 0, so we should make + 1
        int fromPosition = fromIndex + 1;
        int toPosition = toIndex + 1;

        int movedCounterId = playerScore.getId();
        final SparseIntArray positionMap = buildPositionUpdate(playerScoreList, movedCounterId, fromPosition, toPosition);

        repository.modifyPositionBatch(positionMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(e -> Timber.e(e, "modifyPosition playerScore"))
                .onErrorComplete()
                .subscribe();

    }

    private SparseIntArray buildPositionUpdate(@NonNull List<PlayerScore> playerScoreList, int movedCounterId, int fromPosition, int toPosition) {
        int smallerPosition = Math.min(fromPosition, toPosition);
        int largerPosition = Math.max(fromPosition, toPosition);
        int moveStep = toPosition > fromPosition ? -1 : 1;
        final SparseIntArray positionMap = new SparseIntArray();

        for (int i = 0; i < playerScoreList.size(); i++) {

            int position = playerScoreList.get(i).getPosition();

            if (position >= smallerPosition && position <= largerPosition) {
                int id = playerScoreList.get(i).getId();
                int newPosition = id == movedCounterId ? toPosition : position + moveStep;
                positionMap.append(id, newPosition);
            }
        }
        return positionMap;
    }

    void resetCounter(PlayerScore playerScore) {
        repository.setCount(playerScore.getId(), 0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }*/

    private void shuffleColors(String[] arr) {
        Collections.shuffle(Arrays.asList(arr));
    }

    private String getNextColor(int size) {
        if (size < colors.length) {
            return colors[size];
        } else {
            return colors[size % colors.length];
        }
    }

    public SnackbarMessage getSnackbarMessage() {
        return snackbarMessage;
    }

    private void showSnackbarMessage(int value) {
        snackbarMessage.setValue(value);
    }
}