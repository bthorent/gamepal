package com.benthofactory.gamepal.ui.settings;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.LibraryGame;
import com.benthofactory.gamepal.utils.LocalSettings;
import com.benthofactory.gamepal.utils.donate.DonateDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

public class SettingsFragment extends Fragment {

    private View mainView;
    private TextView nameInAppValueTextView;
    private TextView selectionModeValueTextView;
    private TextView selectionNumberValueTextView;
    private SwitchCompat createDefaultPlayerSwitch;
    private SwitchCompat vibrationActivatedSwitch;

    public SettingsFragment() {}

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_settings, container, false);

        mainView.findViewById(R.id.name_in_app).setOnClickListener(v -> {
            final MaterialDialog md = new MaterialDialog.Builder(requireActivity())
                    .content(R.string.name_in_app)
                    .inputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE | InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT)
                    .positiveText(R.string.common_set)
                    .negativeText(R.string.common_cancel)
                    .input(requireActivity().getResources().getString(R.string.your_name_hint), null, true,
                            (dialog, input) -> {
                                    String nameProvided = (dialog.getInputEditText()).getText().toString();
                                    if(nameProvided != null && ! nameProvided.isEmpty()) {
                                        LocalSettings.saveNameInApp(nameProvided);
                                        refreshDisplay();
                                    }
                            })
                    .build();
            EditText editText = md.getInputEditText();
            if (editText != null) {
                editText.setOnEditorActionListener((textView, actionId, event) -> {
                    if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                        View positiveButton = md.getActionButton(DialogAction.POSITIVE);
                        positiveButton.callOnClick();
                    }
                    return false;
                });
            }
            md.show();
        });
        nameInAppValueTextView = mainView.findViewById(R.id.name_in_app_value);

        mainView.findViewById(R.id.default_mode_section).setOnClickListener(v -> {
            new MaterialDialog.Builder(requireActivity())
                .title("Selection mode")
                .items(R.array.select_mode_entries)
                .itemsCallbackSingleChoice((LocalSettings.isPersonDefaultSelectionMode() ? 0 : 1), (dialog, view, which, text) -> {
                    switch(which) {
                        case 0:
                            LocalSettings.saveDefaultPlayersSelectionMode(LocalSettings.SELECTION_MODE_PERSON);
                            refreshDisplay();
                            break;
                        case 1:
                            LocalSettings.saveDefaultPlayersSelectionMode(LocalSettings.SELECTION_MODE_GROUP);
                            refreshDisplay();
                            break;
                    }
                    return true;
                })
                .positiveText("select")
                .show();
        });
        selectionModeValueTextView = mainView.findViewById(R.id.selection_mode_text);

        mainView.findViewById(R.id.default_number_section).setOnClickListener(v -> {
            new MaterialDialog.Builder(requireActivity())
                    .title("Selection number")
                    .items(R.array.select_number_entries)
                    .itemsCallbackSingleChoice(LocalSettings.getDefaultPlayersSelectionNumber()-1, (dialog, view, which, text) -> {
                        LocalSettings.saveDefaultPlayersSelectionNumber(which+1);
                        refreshDisplay();
                        return true;
                    })
                    .positiveText("select")
                    .show();
        });
        selectionNumberValueTextView = mainView.findViewById(R.id.selection_number_text);

        vibrationActivatedSwitch = mainView.findViewById(R.id.sw_vibrate_activated);
        vibrationActivatedSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> LocalSettings.saveVibrationUponSelection(isChecked));

        createDefaultPlayerSwitch = mainView.findViewById(R.id.sw_create_player_for_new_games);
        createDefaultPlayerSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> LocalSettings.saveDefaultPlayerCreationForNewGames(isChecked));

        mainView.findViewById(R.id.delete_all_games).setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage("You're about to delete all games stored. Are you sure you want to proceed ?")
                    .setPositiveButton(R.string.dialog_yes, (dialog, l1) -> {
                        DatabaseHolder.database().gamesDao().deleteAllGames();
                        DatabaseHolder.database().gamesDao().deleteAllPlayerScores();
                        Snackbar.make(getView(), "All games deleted !", Snackbar.LENGTH_LONG).show();
                        dialog.dismiss();
                    })
                    .setNegativeButton(R.string.dialog_no, (dialog, l2) -> dialog.dismiss());
            builder.create().show();
        });

        mainView.findViewById(R.id.feedback_section).setOnClickListener(v -> startEmailClient());
        mainView.findViewById(R.id.share_section).setOnClickListener(v -> shareApp());
        mainView.findViewById(R.id.rate_section).setOnClickListener(v -> rateApp());
        mainView.findViewById(R.id.donate_section).setOnClickListener(v -> {
            DonateDialog dialog = new DonateDialog();
            dialog.show(requireActivity().getSupportFragmentManager(), "donate");
        });

        mainView.findViewById(R.id.delete_games_lib_action).setOnClickListener(v -> DatabaseHolder.database().gamesDao().deleteAllLibraryGames());

        mainView.findViewById(R.id.fill_games_lib_action).setOnClickListener(v -> {
            LibraryGame game = new LibraryGame("The crew");
            game.setCategory("Expert");
            game.setGameDuration("20");
            game.setNumberOfPlayers("3-5");
            game.setFreeDescription("Jeu de pli coopératif, nommé en 2020 au Spiel des Jahres dans la thématique de la découverte spatiale. Prenant et addictif.");
            game.setRating(4);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Jamaica");
            game.setCategory("Famille");
            game.setGameDuration("60");
            game.setNumberOfPlayers("2-6");
            game.setFreeDescription("Jeu de course de bateau de pirates avec gestion de marchandises toujours en flux tendu !");
            game.setRating(4);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Dice forge");
            game.setCategory("Famille");
            game.setGameDuration("45");
            game.setNumberOfPlayers("2-4");
            game.setFreeDescription("Jeu de dice building très original.");
            game.setRating(3);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Yamatai");
            game.setCategory("Famille");
            game.setGameDuration("60");
            game.setNumberOfPlayers("2-4");
            game.setFreeDescription("Jeu de placement de bateau et de contrôle de ressources.");
            game.setRating(4);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Deep Sea Adventure");
            game.setCategory("Ambiance");
            game.setGameDuration("30");
            game.setNumberOfPlayers("2-6");
            game.setFreeDescription("Jeu de stop ou encore avec système intelligent de réserve d'oxygène commune pour plus d'interaction :) !");
            game.setRating(3);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Abyss");
            game.setCategory("Famille");
            game.setGameDuration("45");
            game.setNumberOfPlayers("2-4");
            game.setFreeDescription("Influence et manigance à la cour des atlantes ! Des choix simples à chaque tour et beaucoup de plaisir à jouer !");
            game.setRating(4);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Not Alone");
            game.setCategory("Ambiance");
            game.setGameDuration("30");
            game.setNumberOfPlayers("2-7");
            game.setFreeDescription("Jeu de traque asymétrique sur une planète pas si hospitalière.");
            game.setRating(4);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Kingdomino");
            game.setCategory("Famille");
            game.setGameDuration("15");
            game.setNumberOfPlayers("2-4");
            game.setFreeDescription("Jeu de construction / optimisation de royaume à base de dominos.");
            game.setRating(4);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Codenames");
            game.setCategory("Ambiance");
            game.setGameDuration("15");
            game.setNumberOfPlayers("2-8");
            game.setFreeDescription("Messages codés entre deux équipes d'espions !");
            game.setRating(4);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("7 Wonders Duel");
            game.setCategory("Famille");
            game.setGameDuration("30");
            game.setNumberOfPlayers("2");
            game.setFreeDescription("Dans 7 Wonders : Duel, vous aurez trois ages pour développer votre civilisation en placant devant vous des cartes qui produisent des ressources, en développant l'aspect militaire ou scientifique de votre civilisation ou en bâtissant votre merveille. Mais cette version est jouable uniquement à deux joueurs.");
            game.setRating(4);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Five Tribes");
            game.setCategory("Expert");
            game.setGameDuration("60");
            game.setNumberOfPlayers("2-4");
            game.setFreeDescription("Five Tribes s'appuie sur une longue tradition de jeux de style allemand qui mettent en vedette des meeples en bois. Ici, dans une tournure unique sur le genre désormais standard du \"placement ouvrier\", le jeu commence avec les pions déjà en place - et les joueurs doivent les manœuvrer intelligemment sur les villages, les marchés, les oasis et les lieux sacrés qui composent Naqala. Comment, quand et où vous placez ces cinq tribus d'Assassins, d'Aînés, de bâtisseurs, de marchands et de Vizirs déterminent votre victoire ou votre échec.");
            game.setRating(5);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Lords of Xidit");
            game.setCategory("Expert");
            game.setGameDuration("90");
            game.setNumberOfPlayers("3-5");
            game.setFreeDescription("Lords of Xidit est un jeu de programmation simultanée où, pour mener à bien vos objectifs, vous devrez planifier vos actions, tout en anticipant celles de vos adversaires. La tension atteindra son apogée en fin de partie, où le système unique de décompte par élimination vous réservera bien des surprises…");
            game.setRating(5);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Via Nebula");
            game.setCategory("Famille");
            game.setGameDuration("45");
            game.setNumberOfPlayers("2-4");
            game.setFreeDescription("Dans Via Nebula on incarne des architectes aventuriers, ou archituriers, dans un royaume fantastique en prise avec une brume magique et collante. Le but? Quand le premier architurier sera parvenu à construire tous ses bâtiments, la partie prendra fin et un décompte des points aura lieu: points pour les mines ouvertes, pour les bâtiments, pour les tuiles posées, un bonus pour le joueur qui aura été le premier à construire tous ses bâtiments.");
            game.setRating(5);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("Kanagawa");
            game.setCategory("Famille");
            game.setGameDuration("45");
            game.setNumberOfPlayers("2-4");
            game.setFreeDescription("Réaliser la plus belle estampe et devenez le plus prestigieux élève du peintre Hokusai !");
            game.setRating(5);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            game = new LibraryGame("King of Tokyo");
            game.setCategory("Famille");
            game.setGameDuration("30");
            game.setNumberOfPlayers("2-6");
            game.setFreeDescription("Incarnez un monstre et affrontez ceux des autres jusqu'à devenir le King of Tokyo... Pour ça, usez de cartes et surtout de vos dés !");
            game.setRating(5);
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
        });

        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshDisplay();
        // No keyboard here if present before
        hideKeyboardFrom(getContext(), mainView);
    }

    private void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void refreshDisplay() {
        nameInAppValueTextView.setText((LocalSettings.getNameInApp()!=null ? LocalSettings.getNameInApp() : getString(R.string.settings_not_set)));
        selectionModeValueTextView.setText(LocalSettings.isPersonDefaultSelectionMode() ? getString(R.string.selection_mode_person) : getString(R.string.selection_mode_group));
        selectionNumberValueTextView.setText(getString(R.string.select_default_text, LocalSettings.getDefaultPlayersSelectionNumber()));
        vibrationActivatedSwitch.setChecked(LocalSettings.isVibrationUponSelectionActivated());
        createDefaultPlayerSwitch.setChecked(LocalSettings.hasDefaultPlayerCreationForNewGamesActivated());
    }

    private void startEmailClient() {
        final String title = getString(R.string.app_name);

        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "b.thorent@gmail.com", null));
        intent.putExtra(Intent.EXTRA_EMAIL, "b.thorent@gmail.com");
        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        if (intent.resolveActivity(requireActivity().getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Snackbar.make(getView(), R.string.error_no_email_client, Snackbar.LENGTH_LONG).show();
        }
    }

    private void shareApp() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_snippet) + requireActivity().getPackageName());
        Intent chooserIntent = Intent.createChooser(shareIntent, getString(R.string.setting_share));
        chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(chooserIntent);
    }

    private void rateApp() {
        Activity activity = requireActivity();
        Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            activity.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            Uri playStoreUri = Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName());
            activity.startActivity(new Intent(Intent.ACTION_VIEW, playStoreUri));
        }
    }

}
