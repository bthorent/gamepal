package com.benthofactory.gamepal.ui.selectplayers;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.utils.LocalSettings;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;


public class SelectPlayersBottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private DialogInterface.OnDismissListener onDismissListener;
    private String playersSelectionMode;
    private int playersSelectionNumber;
    private ToggleButton modePerson, modeGroup, numberOne, numberTwo, numberThree, numberFour, numberFive;
    private TextView incompatibleSettingMessage;
    private MaterialButton updateSelectionSettingsBtn;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View contentView = inflater.inflate(R.layout.fragment_select_player_sheet, container, false);

        incompatibleSettingMessage = contentView.findViewById(R.id.invalid_setup_message);
        modePerson = contentView.findViewById(R.id.tb_mode_person);
        modeGroup = contentView.findViewById(R.id.tb_mode_group);
        numberOne = contentView.findViewById(R.id.tb_number_one);
        numberTwo = contentView.findViewById(R.id.tb_number_two);
        numberThree = contentView.findViewById(R.id.tb_number_three);
        numberFour = contentView.findViewById(R.id.tb_number_four);
        numberFive = contentView.findViewById(R.id.tb_number_five);
        updateSelectionSettingsBtn = contentView.findViewById(R.id.btn_save);

        playersSelectionMode = LocalSettings.getGroupSelectionMode();
        playersSelectionNumber = LocalSettings.getPlayersSelectionNumber();

        modePerson.setOnClickListener(this);
        modeGroup.setOnClickListener(this);
        numberOne.setOnClickListener(this);
        numberTwo.setOnClickListener(this);
        numberThree.setOnClickListener(this);
        numberFour.setOnClickListener(this);
        numberFive.setOnClickListener(this);

        updateSelectionSettingsBtn.setOnClickListener(v -> {
            refreshSelectionSetup(true);
            onDismiss(Objects.requireNonNull(getDialog()));
        });

        //((ToggleButton) contentView.findViewById(R.id.person_toggle_test))

        refreshSelectionSetup(false);
        return contentView;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        if (onDismissListener != null) {
            refreshSelectionSetup(false);
            onDismissListener.onDismiss(dialog);
        }
    }

    void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        onDismissListener = listener;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() != null && getDialog().getWindow() != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int oldFlags = getDialog().getWindow().getDecorView().getSystemUiVisibility();
            // Apply the state flags in priority order
            int newFlags = oldFlags;
            if (newFlags != oldFlags) {
                getDialog().getWindow().getDecorView().setSystemUiVisibility(newFlags);
            }
        }
    }

    @Override
    public void onClick(View v) {
        hideIncompatibleSetupMessage();
        switch (v.getId()) {
            case R.id.tb_mode_person:
                playersSelectionMode = LocalSettings.SELECTION_MODE_PERSON;
                refreshSelectionSetup(false);
                break;
            case R.id.tb_mode_group:
                // Cannot have a group of 1 as selection option, not taking it into account and displaying message
                if(numberOne.isChecked()){
                    modeGroup.setChecked(false);
                    displayIncompatibleSetupMessage();
                } else {
                    playersSelectionMode = LocalSettings.SELECTION_MODE_GROUP;
                    refreshSelectionSetup(false);
                }
                break;
            case R.id.tb_number_one:
                // Cannot have a group of 1 as selection option, not taking it into account and displaying message
                if(modeGroup.isChecked()){
                    numberOne.setChecked(false);
                    displayIncompatibleSetupMessage();
                } else {
                    playersSelectionNumber = 1;
                    refreshSelectionSetup(false);
                }
                break;
            case R.id.tb_number_two:
                playersSelectionNumber = 2;
                refreshSelectionSetup(false);
                break;
            case R.id.tb_number_three:
                playersSelectionNumber = 3;
                refreshSelectionSetup(false);
                break;
            case R.id.tb_number_four:
                playersSelectionNumber = 4;
                refreshSelectionSetup(false);
                break;
            case R.id.tb_number_five:
                playersSelectionNumber = 5;
                refreshSelectionSetup(false);
                break;
        }
    }

    private void displayIncompatibleSetupMessage() {
        incompatibleSettingMessage.setVisibility(View.VISIBLE);
    }

    private void hideIncompatibleSetupMessage() {
        incompatibleSettingMessage.setVisibility(View.GONE);
    }

    private void refreshSelectionSetup(boolean storeInDB) {
        if (storeInDB) {
            LocalSettings.savePlayersSelectionMode(playersSelectionMode);
            LocalSettings.savePlayersSelectionNumber(playersSelectionNumber);
        }
        switch (playersSelectionMode) {
            case LocalSettings.SELECTION_MODE_GROUP:
                modePerson.setChecked(false);
                modeGroup.setChecked(true);
                break;
            default:
                modePerson.setChecked(true);
                modeGroup.setChecked(false);
                break;
        }
        switch (playersSelectionNumber) {
            case 2:
                numberOne.setChecked(false);
                numberTwo.setChecked(true);
                numberThree.setChecked(false);
                numberFour.setChecked(false);
                numberFive.setChecked(false);
                break;
            case 3:
                numberOne.setChecked(false);
                numberTwo.setChecked(false);
                numberThree.setChecked(true);
                numberFour.setChecked(false);
                numberFive.setChecked(false);
                break;
            case 4:
                numberOne.setChecked(false);
                numberTwo.setChecked(false);
                numberThree.setChecked(false);
                numberFour.setChecked(true);
                numberFive.setChecked(false);
                break;
            case 5:
                numberOne.setChecked(false);
                numberTwo.setChecked(false);
                numberThree.setChecked(false);
                numberFour.setChecked(false);
                numberFive.setChecked(true);
                break;
            default:
                numberOne.setChecked(true);
                numberTwo.setChecked(false);
                numberThree.setChecked(false);
                numberFour.setChecked(false);
                numberFive.setChecked(false);
                break;
        }
    }
}