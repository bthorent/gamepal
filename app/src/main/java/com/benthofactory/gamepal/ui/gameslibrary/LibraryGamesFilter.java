package com.benthofactory.gamepal.ui.gameslibrary;

import android.widget.Filter;

import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.Game;
import com.benthofactory.gamepal.model.LibraryGame;
import com.benthofactory.gamepal.model.PlayerScore;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LibraryGamesFilter extends Filter {

    LibraryGameRecyclerViewAdapter adapter;
    ArrayList<LibraryGame> filterList;

    Map<String, String> map = new HashMap<>();
    public static String CONSTRAINT_NAME = "CONSTRAINT_NAME";
    public static String CONSTRAINT_MIN_NBR_OF_PLAYERS = "CONSTRAINT_MIN_NBR_OF_PLAYERS";
    public static String CONSTRAINT_MAX_NBR_OF_PLAYERS = "CONSTRAINT_MAX_NBR_OF_PLAYERS";
    public static String CONSTRAINT_MIN_DURATION = "CONSTRAINT_MIN_DURATION";
    public static String CONSTRAINT_MAX_DURATION = "CONSTRAINT_MAX_DURATION";

    public LibraryGamesFilter(ArrayList<LibraryGame> filterList, LibraryGameRecyclerViewAdapter adapter)
    {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    public void addConstraint(String key, String value){
        map.put(key, value);
    }

    public void resetConstraints() {
        map = new HashMap<>();
    }

    @Override
    protected FilterResults performFiltering(CharSequence s) {
        FilterResults results = new FilterResults();

        if(!map.keySet().isEmpty())
        {
            ArrayList<LibraryGame> filteredGames = new ArrayList<>();

            for (LibraryGame game: filterList) {

                boolean isEligible = true;

                // Name constraint
                if(map.containsKey(CONSTRAINT_NAME)) {
                    String normalizedConstraint = normalizeString(map.get(CONSTRAINT_NAME));
                    isEligible = normalizeString(game.getName()).contains(normalizedConstraint);
                }

                // Nbr players constraint
                if(isEligible && map.containsKey(CONSTRAINT_MIN_NBR_OF_PLAYERS)) {
                    isEligible = game.appliesToNbrOfPlayers(Integer.parseInt(map.get(CONSTRAINT_MIN_NBR_OF_PLAYERS)));
                }
                if(isEligible && map.containsKey(CONSTRAINT_MAX_NBR_OF_PLAYERS)) {
                    isEligible = game.appliesToNbrOfPlayers(Integer.parseInt(map.get(CONSTRAINT_MAX_NBR_OF_PLAYERS)));
                }

                // Duration constraint
                if(isEligible && map.containsKey(CONSTRAINT_MIN_DURATION)) {
                    isEligible = game.appliesToMinDuration(Integer.parseInt(map.get(CONSTRAINT_MIN_DURATION)));
                }
                if(isEligible && map.containsKey(CONSTRAINT_MAX_DURATION)) {
                    isEligible = game.appliesToMaxDuration(Integer.parseInt(map.get(CONSTRAINT_MAX_DURATION)));
                }

                if (isEligible) {
                    filteredGames.add(game);
                }
            }

            results.count = filteredGames.size();
            results.values = filteredGames;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.setGamesList((ArrayList<LibraryGame>) results.values);
        adapter.notifyDataSetChanged();
    }

    private static String normalizeString(String stringToNormalize) {
        return Normalizer.normalize(stringToNormalize.toUpperCase(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }
}
