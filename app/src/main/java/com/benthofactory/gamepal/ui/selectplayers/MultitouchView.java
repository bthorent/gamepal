package com.benthofactory.gamepal.ui.selectplayers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Region;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.MotionEvent;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.utils.LocalSettings;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressLint("ViewConstructor")
public class MultitouchView extends View {

    private static final int SIZE = 130;

    private static final int maxPointers = 10;
    private int[] colors = { Color.parseColor("#f44336"), // Red
            Color.parseColor("#4CAF50"), // Green
            Color.parseColor("#2196F3"), // Blue
            Color.parseColor("#FF9800"), // Orange
            Color.parseColor("#009688"), // Teal
            Color.parseColor("#9C27B0"), // Purple
            Color.parseColor("#3F51B5"), // Indigo
            Color.parseColor("#FFEB3B"), // Yellow
            Color.parseColor("#795548"), // Brown
            Color.parseColor("#9E9E9E") // Grey
    };

    private SparseArray<PointF> mActivePointers;
    private SparseIntArray mPointersColors;
    private Paint mPaint;
    private Path mPath;
    private Handler selectionTimerHandler;
    private Handler cleanupTimerHandler;
    private View mView;
    private int mNumberOfFingersOnScreen;

    private SelectPlayersFragment mFragment;

    private ArrayList<List<Integer>> selectedPointerIdGroups;

    public MultitouchView(Context context, SelectPlayersFragment fragment) {
        super(context);
        mView = this;
        mFragment = fragment;
        initView();
    }

    private void initView() {
        selectedPointerIdGroups = null;
        mActivePointers = new SparseArray<>();
        mPointersColors = new SparseIntArray();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPath = new Path();
        mNumberOfFingersOnScreen = 0;
        stopCleanupTimerIfNeeded();
        mFragment.updateTip(mNumberOfFingersOnScreen);
    }

    private Integer findUnusedColor(){
        for (int color : colors) {
            boolean found = false;
            for (int j = 0; j < mPointersColors.size(); j++) {
                if (mPointersColors.valueAt(j) == color) {
                    found = true;
                }
            }
            if (!found) {
                return color;
            }
        }
        // Should never get there
        return colors[0];
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get pointer ID
        int pointerId = event.getPointerId(pointerIndex);

        // get masked (not specific to a pointer) action
        int maskedAction = event.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                if (selectedPointerIdGroups == null) {
                    if (mActivePointers.size()<maxPointers) {
                        // We have a new pointer. Lets add it to the list of pointers
                        PointF f = new PointF();
                        f.x = event.getX(pointerIndex);
                        f.y = event.getY(pointerIndex);
                        mActivePointers.put(pointerId, f);
                        mPointersColors.put(pointerId, findUnusedColor());
                        startSelectionTimerIfNeeded();
                    } else {
                        Snackbar.make(this, "Cannot have more than " + maxPointers + " players at the same time !", Snackbar.LENGTH_LONG).show();
                    }
                }
                // Get number of screens whatever the mode to know when to remove selection screen later
                mNumberOfFingersOnScreen+=1;
                mFragment.updateTip(mNumberOfFingersOnScreen);
                mFragment.hideImageTip();
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                if (selectedPointerIdGroups == null) {
                    for (int size = event.getPointerCount(), i = 0; i < size; i++) {
                        PointF point = mActivePointers.get(event.getPointerId(i));
                        if (point != null) {
                            point.x = event.getX(i);
                            point.y = event.getY(i);
                        }
                    }
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_CANCEL: {
                if (selectedPointerIdGroups == null) {
                    try {
                        mActivePointers.remove(pointerId);
                    } catch (Exception e) {
                        // Do nothing
                    }
                    try {
                        mPointersColors.removeAt(pointerId);
                    } catch (Exception e) {
                        // Do nothing
                    }
                }
                stopSelectionTimerIfNeeded();
                startSelectionTimerIfNeeded();

                // Get number of screens whatever the mode to know when to remove selection screen later
                mNumberOfFingersOnScreen-=1;
                mFragment.updateTip(mNumberOfFingersOnScreen);
                if (selectedPointerIdGroups != null && mNumberOfFingersOnScreen==0) {
                    startCleanupTimer();
                }
                if (mNumberOfFingersOnScreen == 0) {
                    mFragment.showImageTip();
                }
                break;
            }
        }

        invalidate();

        /*if (mActivePointers.size() > mFragment.getNumberMode()) {
            mFragment.hideToolBar();
        } else {
            mFragment.showToolBar();
        }*/

        return true;
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawColor(Color.TRANSPARENT);

        // If no selection yet, draw all identified pointers
        if (selectedPointerIdGroups == null) {// Draw all pointers
            try {
                for (int i = 0; i < mActivePointers.size(); i++) {
                    PointF point = mActivePointers.valueAt(i);
                    if (mFragment.isPersonMode()) {
                        mPaint.setColor(mPointersColors.valueAt(i));
                    } else {
                        mPaint.setColor(ContextCompat.getColor(getContext(), R.color.grey_20));
                    }
                    canvas.drawCircle(point.x, point.y, SIZE, mPaint);
                }
            } catch (Exception e) {
                initView();
            }
        } else { // Otherwise display selected one
            try {
                if (mFragment.isPersonMode()) {
                    for (Integer i : selectedPointerIdGroups.get(0)) {
                        PointF selectedPointer = mActivePointers.valueAt(i);
                        int selectedPointerColor = mPointersColors.valueAt(i);
                        mPaint.setColor(selectedPointerColor);
                        canvas.drawCircle(selectedPointer.x, selectedPointer.y, SIZE, mPaint);
                    }

                    for (Integer i : selectedPointerIdGroups.get(0)) {
                        PointF selectedPointer = mActivePointers.valueAt(i);
                        if (Build.VERSION.SDK_INT >= 26) {
                            mPath.addCircle(selectedPointer.x, selectedPointer.y, SIZE+30,  Path.Direction.CCW);
                            canvas.clipOutPath(mPath);
                        } else {
                            mPath.addCircle(selectedPointer.x, selectedPointer.y, SIZE+30,  Path.Direction.CCW);
                            canvas.clipPath(mPath, Region.Op.DIFFERENCE);
                        }
                    }

                    if (mFragment.getNumberMode()==1){
                        canvas.drawColor(mPointersColors.valueAt(selectedPointerIdGroups.get(0).get(0)));
                    } else {
                        canvas.drawColor(Color.parseColor("#424242"));
                    }
                } else {
                    for (int j=0; j<selectedPointerIdGroups.size(); j++) {
                        int color = colors[j];
                        for (Integer i : selectedPointerIdGroups.get(j)) {
                            PointF selectedPointer = mActivePointers.valueAt(i);
                            mPaint.setColor(color);
                            canvas.drawCircle(selectedPointer.x, selectedPointer.y, SIZE, mPaint);
                        }
                    }

                    for (int i=0; i<mActivePointers.size(); i++) {
                        PointF selectedPointer = mActivePointers.valueAt(i);
                        mPath.addCircle(selectedPointer.x, selectedPointer.y, SIZE+30,  Path.Direction.CCW);
                    }
                    if (Build.VERSION.SDK_INT >= 26) {
                        canvas.clipOutPath(mPath);
                    } else {
                        canvas.clipPath(mPath, Region.Op.DIFFERENCE);
                    }
                    canvas.drawColor(Color.parseColor("#424242"));
                }
            } catch (Exception e) {
                initView();
            }
        }
    }

    private List<Integer> generateRandomInts(int maxInt, int nbrRandomsNeeded) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i=0; i<maxInt; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        List<Integer> result = new ArrayList<>();
        for (int i=0; i<nbrRandomsNeeded; i++) {
            result.add(list.get(i));
        }
        return result;
    }

    private ArrayList<List<Integer>> generateRandomIntGroups(int maxInt, int nbrOfGroups) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i=0; i<maxInt; i++) {
            list.add(i);
        }
        Collections.shuffle(list);

        ArrayList<List<Integer>> groups = new ArrayList<>();
        int size = (int) Math.ceil(list.size() / nbrOfGroups);
        int additionalItems = list.size() - size*nbrOfGroups;
        int additionalIncrement;
        for (int start = 0; start < list.size(); start += size + additionalIncrement) {
            additionalIncrement = ((additionalItems==0) ? 0 : 1);
            int end = Math.min(start+size+additionalIncrement, list.size());
            List<Integer> sublist = list.subList(start, end);
            additionalItems = Math.max(0, additionalItems-1);
            groups.add(sublist);
        }

        return groups;
    }

    private void startSelectionTimerIfNeeded() {
        if (mActivePointers.size() > mFragment.getNumberMode()){
            final int selectionInterval = 3000; // 3 Seconds
            if (selectionTimerHandler != null)
                selectionTimerHandler.removeCallbacksAndMessages(null);
            selectionTimerHandler = new Handler();
            Runnable runnable = () -> {
                if (selectedPointerIdGroups == null){
                    if (mFragment.isPersonMode()) {
                        selectedPointerIdGroups = new ArrayList<>();
                        selectedPointerIdGroups.add(generateRandomInts(mActivePointers.size(), mFragment.getNumberMode()));
                    } else {
                        selectedPointerIdGroups = generateRandomIntGroups(mActivePointers.size(), mFragment.getNumberMode());
                    }

                    if (LocalSettings.isVibrationUponSelectionActivated()) {
                        Vibrator v = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
                        if (v != null) {
                            v.vibrate(800);
                        }
                    }

                    invalidate();
                }
            };
            selectionTimerHandler.postDelayed(runnable, selectionInterval);
        }
    }

    private void stopSelectionTimerIfNeeded() {
        if (selectionTimerHandler != null && mActivePointers.size() <= mFragment.getNumberMode())
            selectionTimerHandler.removeCallbacksAndMessages(null);
    }

    private void startCleanupTimer() {
        final int cleanupInterval = 3000; // 3 Seconds
        cleanupTimerHandler = new Handler();
        Runnable runnable = () -> {
            initView();
            Snackbar.make(mView, "Ready for next selection !", Snackbar.LENGTH_LONG).show();
            invalidate();
        };
        cleanupTimerHandler.postDelayed(runnable, cleanupInterval);
    }

    private void stopCleanupTimerIfNeeded() {
        if (cleanupTimerHandler != null)
            selectionTimerHandler.removeCallbacksAndMessages(null);
    }

}
