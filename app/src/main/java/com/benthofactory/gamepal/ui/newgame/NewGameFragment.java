package com.benthofactory.gamepal.ui.newgame;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.Game;
import com.benthofactory.gamepal.model.PlayerScore;
import com.benthofactory.gamepal.ui.editplayer.EditPlayerActivity;
import com.benthofactory.gamepal.utils.LocalSettings;
import com.benthofactory.gamepal.utils.Utilities;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

public class NewGameFragment extends Fragment implements CounterActionCallback {

    private View contentView;
    private GameViewModel viewModel;
    private AppCompatAutoCompleteTextView gameEditText;
    private TextView gameDateText;
    private TextView gameHourText;
    private RecyclerView recyclerView;
    private ItemTouchHelper itemTouchHelper;
    private boolean isLongPressTipShowed;
    private MaterialDialog longClickDialog;

    private FloatingActionMenu gamesMenu;
    private FloatingActionButton gamesMenuDeleteGame;
    private FloatingActionButton gamesMenuNextRound;
    private FloatingActionButton gamesMenuAddPlayer;
    private FloatingActionButton gamesMenuSaveGame;

    private static final int MODE_INCREASE_VALUE = 1;
    private static final int MODE_DECREASE_VALUE = 2;
    private static final int MODE_SET_VALUE = 3;

    private PlayerScoresAdapter playerScoresAdapter;
    private int oldListSize;

    private static final String INCREASE_VALUE_CLICK = "increase_value_click";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        contentView = inflater.inflate(R.layout.fragment_new_game, container, false);

        gameEditText = contentView.findViewById(R.id.et_game_name);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, DatabaseHolder.database().gamesDao().getAllUniqueGameNames());
        gameEditText.setThreshold(1);
        gameEditText.setAdapter(adapter);


        gameDateText = contentView.findViewById(R.id.date_text);
        gameHourText = contentView.findViewById(R.id.hour_text);

        recyclerView = contentView.findViewById(R.id.recycler_view);
        recyclerView.setItemAnimator(new ChangePlayerScoreValueAnimator());

        viewModel = new GameViewModel(requireActivity().getApplication(), getContext());

        contentView.findViewById(R.id.emptyGame).setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage("Create new game ?")
                    .setPositiveButton(R.string.dialog_yes, (dialog, l1) -> {
                        viewModel.createGame();
                        updateDisplay();
                        subscribeUi();
                        dialog.dismiss();
                    })
                    .setNegativeButton(R.string.dialog_no, (dialog, l2) -> dialog.dismiss());
            builder.create().show();
        });

        gamesMenu = contentView.findViewById(R.id.games_menu);
        gamesMenuDeleteGame = contentView.findViewById(R.id.games_menu_delete_game);
        gamesMenuNextRound = contentView.findViewById(R.id.games_menu_next_round);
        gamesMenuAddPlayer = contentView.findViewById(R.id.games_menu_add_player);
        gamesMenuSaveGame = contentView.findViewById(R.id.games_menu_save_game);

        gamesMenuAddPlayer.setOnClickListener(v -> {
            viewModel.addPlayerScore();
            gamesMenu.close(true);
        });

        gamesMenuNextRound.setOnClickListener(v -> {
            viewModel.moveToNextRound();
            subscribeUi();
            updateDisplay();
            Snackbar.make(contentView, "Next round", Snackbar.LENGTH_LONG).show();
            gamesMenu.close(true);
        });

        gamesMenuDeleteGame.setOnClickListener(v -> {
            deleteGame();
            gamesMenu.close(true);
        });

        gamesMenuSaveGame.setOnClickListener(v -> {
            hideKeyboardFrom(getContext(), contentView);
            saveGameAndUpdateDisplay();
            gamesMenu.close(true);
        });

        //contentView.findViewById(R.id.date_section_edit_clickable_area).setOnClickListener(v -> requestDateSelection());
        //contentView.findViewById(R.id.hour_section_edit_clickable_area).setOnClickListener(v -> requestTimeSelection());

        return contentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        subscribeUi();

        if (viewModel.getDraftGame() != null) {
            PlayerScoresAdapter playerScoresAdapter = new PlayerScoresAdapter(this, true);
            playerScoresAdapter.setPlayerScoresList(viewModel.getDraftGame().getPlayerScores());
            recyclerView.setAdapter(playerScoresAdapter);
        }

        updateDisplay();
    }

    private void subscribeUi() {
        viewModel = new GameViewModel(requireActivity().getApplication(), getContext());
        playerScoresAdapter = new PlayerScoresAdapter(this, true);

        if (viewModel.getPlayerScores() != null) {
            viewModel.getPlayerScores().observe(getViewLifecycleOwner(), counters -> {
                if (counters != null) {
                    final int size = counters.size();
                    playerScoresAdapter.notifyDataSetChanged();
                    if (size > 0) {
                        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

                    } else {
                        recyclerView.setLayoutManager(new SpanningLinearLayoutManager(requireContext()));
                    }

                    playerScoresAdapter.setPlayerScoresList(counters);
                    if (size > oldListSize && oldListSize > 0) {
                        recyclerView.smoothScrollToPosition(size);
                    }
                    recyclerView.post(() -> {
                                recyclerView.setAdapter(playerScoresAdapter);
                                ItemTouchHelper.Callback callback = new ItemDragHelperCallback(playerScoresAdapter);
                                itemTouchHelper = new ItemTouchHelper(callback);
                                itemTouchHelper.attachToRecyclerView(recyclerView);
                            }
                    );
                    oldListSize = size;
                }
            });
        }
        viewModel.getSnackbarMessage().observe(getViewLifecycleOwner(), (Observer<Integer>) resourceId -> Snackbar.make(recyclerView, getString(resourceId), Snackbar.LENGTH_SHORT)
                .setAction(R.string.snackbar_action_one_more, v -> viewModel.addPlayerScore()).show());
        gameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                viewModel.updateGameName(Objects.requireNonNull(gameEditText.getText()).toString());
            }
        });
    }

    private void updateDisplay() {
        if (viewModel.getDraftGame() == null) {
            contentView.findViewById(R.id.emptyGame).setVisibility(View.VISIBLE);
            contentView.findViewById(R.id.currentGameActive).setVisibility(View.GONE);
        } else {
            contentView.findViewById(R.id.emptyGame).setVisibility(View.GONE);
            contentView.findViewById(R.id.currentGameActive).setVisibility(View.VISIBLE);
            gameEditText.setText(viewModel.getDraftGame().getName());
        }
        contentView.findViewById(R.id.calendar_section).setVisibility(View.GONE);
        contentView.findViewById(R.id.delete_game_btn).setVisibility(View.GONE);
        recyclerView.setClipToPadding(false);
        recyclerView.setPadding(0, 0,0,200);
    }

    private void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void saveGameAndUpdateDisplay() {
        Game gameUnderCreation = viewModel.getDraftGame();
        boolean isValidGame = gameUnderCreation.validate();
        if (isValidGame) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(R.string.dialog_confirmation_question)
                    .setPositiveButton(R.string.dialog_yes, (dialog, l1) -> {
                        viewModel.saveGame();
                        subscribeUi();
                        updateDisplay();
                        Snackbar.make(contentView, "Game created !", Snackbar.LENGTH_LONG).show();
                        dialog.dismiss();
                        ((ViewPager) getActivity().findViewById(R.id.pager)).setCurrentItem(1, true);
                    })
                    .setNegativeButton(R.string.dialog_no, (dialog, l2) -> dialog.dismiss());
            builder.create().show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(R.string.invalid_game)
                    .setPositiveButton(R.string.common_ok, (dialog, l1) -> {
                        dialog.dismiss();
                    });
            builder.create().show();
        }
    }

    public void deleteGame() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Delete current game ?")
                .setPositiveButton(R.string.dialog_yes, (dialog, l1) -> {
                    viewModel.cancelGameCreation();
                    subscribeUi();
                    updateDisplay();
                    dialog.dismiss();
                })
                .setNegativeButton(R.string.dialog_no, (dialog, l2) -> dialog.dismiss());
        builder.create().show();
    }

    @Override
    public void onSingleClick(PlayerScore playerScore, int position, int mode) {
        if (mode == MODE_DECREASE_VALUE) {
            viewModel.decreaseCounter(playerScore, -1);
            showLongPressHint();
        } else if (mode == MODE_INCREASE_VALUE) {
            viewModel.increaseCounter(playerScore, 1);
            showLongPressHint();
        } else if (mode == MODE_SET_VALUE) {
            showCounterStepDialog(playerScore, position, mode);
        }
    }

    @Override
    public void onLongClick(PlayerScore playerScore, int position, int mode) {
        if (mode == MODE_SET_VALUE) {
            final MaterialDialog md = new MaterialDialog.Builder(requireActivity())
                    .content(playerScore.getName())
                    .inputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED)
                    .positiveText(R.string.common_set)
                    .negativeText(R.string.common_cancel)
                    .alwaysCallInputCallback()
                    .input("" + playerScore.getScore(), null, false, (dialog, input) -> {

                    })
                    .onNeutral((dialog, which) -> viewModel.resetCounter(playerScore))
                    .onPositive((dialog, which) -> {
                        EditText editText = dialog.getInputEditText();
                        if (editText != null) {
                            Integer value = Utilities.parseInt(editText.getText().toString());
                            viewModel.modifyCurrentValue(playerScore, value);
                            playerScoresAdapter.notifyItemChanged(position, INCREASE_VALUE_CLICK);
                            dialog.dismiss();
                        }
                    })
                    .build();
            EditText editText = md.getInputEditText();
            if (editText != null) {
                editText.setOnEditorActionListener((textView, actionId, event) -> {
                    if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                        View positiveButton = md.getActionButton(DialogAction.POSITIVE);
                        positiveButton.callOnClick();
                    }
                    return false;
                });
            }
            md.show();
        } else {
            showCounterStepDialog(playerScore, position, mode);
        }
    }

    @SuppressLint("SetTextI18n")
    private void showCounterStepDialog(PlayerScore playerScore, int position, int mode) {
        final MaterialDialog.Builder builder = new MaterialDialog.Builder(requireActivity());
        final Observer<PlayerScore> counterObserver = c -> {
            if (longClickDialog != null && c != null) {
                longClickDialog.getTitleView().setText(c.getName());
            }
        };
        boolean isIncrease = mode != MODE_DECREASE_VALUE;
        final LiveData<PlayerScore> liveData = viewModel.getCounterLiveData(playerScore.getId());
        liveData.observe(this, counterObserver);

        //int layoutId = isIncrease ? R.layout.dialog_counter_step_increase : R.layout.dialog_counter_step_decrease;
        @SuppressLint("InflateParams") final View contentView = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_counter_step_update, null, false);

        String btnSign = isIncrease ? "+" : "-";
        String reverseBtnSign = isIncrease ? "-" : "+";

        ((TextView) contentView.findViewById(R.id.current_score)).setText(getString(R.string.current_score_fyi, playerScore.getScore()));
        ((TextView) contentView.findViewById(R.id.increase_decrease_indicator)).setText(btnSign);
        ((MaterialButton) contentView.findViewById(R.id.btn_confirm_update)).setText(isIncrease ? R.string.add_to_score : R.string.remove_from_score);
        TextView invalidNegativeValue = contentView.findViewById(R.id.invalid_value_message);
        invalidNegativeValue.setText(isIncrease ? R.string.impossible_value_cannot_add_negative : R.string.impossible_value_cannot_remove_negative);

        MaterialButton btnConfirmUpdate = contentView.findViewById(R.id.btn_confirm_update);
        EditText updateScoreEditText = contentView.findViewById(R.id.et_add_custom_value);

        updateScoreEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (updateScoreEditText.getText().toString().isEmpty()) {
                    invalidNegativeValue.setVisibility(View.GONE);
                    btnConfirmUpdate.setEnabled(true);
                } else {
                    boolean isInvalidInput;
                    try {
                        isInvalidInput = Integer.parseInt(updateScoreEditText.getText().toString()) <0;
                    } catch (Exception e) {
                        isInvalidInput = false;
                    }
                    if (isInvalidInput) {
                        invalidNegativeValue.setVisibility(View.VISIBLE);
                        btnConfirmUpdate.setEnabled(false);
                    } else {
                        invalidNegativeValue.setVisibility(View.GONE);
                        btnConfirmUpdate.setEnabled(true);
                    }
                }
                updateScoreEditText.setSelection(updateScoreEditText.getText().length());
            }
        });

        btnConfirmUpdate.setOnClickListener(v -> {
            if (isIncrease) {
                viewModel.increaseCounter(playerScore, Integer.parseInt(updateScoreEditText.getText().toString()));
            } else {
                viewModel.decreaseCounter(playerScore, -Integer.parseInt(updateScoreEditText.getText().toString()));
            }
            playerScoresAdapter.notifyItemChanged(position, isIncrease ? INCREASE_VALUE_CLICK : MODE_DECREASE_VALUE);
            longClickDialog.dismiss();
        });

        View.OnClickListener buttonUpdateListener = v -> {
            int value = Integer.parseInt(((TextView)v).getText().toString());
            int currentIncreaseDecreaseValue;
            try {
                currentIncreaseDecreaseValue = Integer.parseInt(updateScoreEditText.getText().toString());
            } catch (Exception e) {
                currentIncreaseDecreaseValue = 0;
            }
            int newValue = isIncrease ? currentIncreaseDecreaseValue + value : currentIncreaseDecreaseValue - value;

            if (newValue <0) {
                invalidNegativeValue.setVisibility(View.VISIBLE);
            } else {
                updateScoreEditText.setText(String.valueOf(newValue));
            }
        };

        TextView btnOne = (contentView.findViewById(R.id.btn_one_text));
        TextView btnTwo = (contentView.findViewById(R.id.btn_two_text));
        TextView btnThree = (contentView.findViewById(R.id.btn_three_text));
        TextView btnFour = (contentView.findViewById(R.id.btn_four_text));

        // Update button values
        btnOne.setText(reverseBtnSign + 1);
        btnTwo.setText(btnSign + 1);
        btnThree.setText(btnSign + 5);
        btnFour.setText(btnSign + 10);

        // Set buttons listener
        btnOne.setOnClickListener(buttonUpdateListener);
        btnTwo.setOnClickListener(buttonUpdateListener);
        btnThree.setOnClickListener(buttonUpdateListener);
        btnFour.setOnClickListener(buttonUpdateListener);

        final EditText editText = contentView.findViewById(R.id.et_add_custom_value);
        builder.customView(contentView, false);
        builder.title(R.string.dialog_current_value_title);
        builder.dismissListener(dialogInterface -> liveData.removeObserver(counterObserver));
        longClickDialog = builder.build();
        longClickDialog.show();

        editText.post(() -> {
            editText.requestFocus();

            InputMethodManager inputMethodManager = (InputMethodManager) requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
            }
        });
    }

    @Override
    public void onNameClick(PlayerScore playerScore) {
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.autocomplete_text_view_dialog, null);

        ((TextView) view.findViewById(R.id.title)).setText(R.string.player_details_name);

        AutoCompleteTextView nameAutocomplete = view.findViewById(R.id.player_name);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, DatabaseHolder.database().gamesDao().getAllUniquePlayerNames());
        nameAutocomplete.setAdapter(adapter);
        nameAutocomplete.setThreshold(1);

        nameAutocomplete.setText(playerScore.getName());

        nameAutocomplete.setHint(requireActivity().getResources().getString(R.string.hint_new_player_name));

        final MaterialDialog md = new MaterialDialog.Builder(requireActivity())
                .customView(view, false)
                .positiveText(R.string.common_set)
                .negativeText(R.string.common_cancel)
                .onPositive((dialog, input) -> viewModel.modifyName(playerScore, nameAutocomplete.getText().toString()))
                .build();

        nameAutocomplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    md.getActionButton(DialogAction.POSITIVE).setEnabled(false);
                } else {
                    md.getActionButton(DialogAction.POSITIVE).setEnabled(true);
                }
            }
        });

        md.show();
    }

    @Override
    public void onEditClick(PlayerScore playerScore) {
        EditPlayerActivity.start(getActivity(), playerScore);
    }

    private void showLongPressHint() {
        if (!isLongPressTipShowed) {
            Handler handler = new Handler();
            handler.postDelayed(() -> Snackbar.make(contentView, R.string.message_you_can_use_long_press, Snackbar.LENGTH_LONG).show(), 500);
            LocalSettings.setLongPressTipShowed();
            isLongPressTipShowed = true;
        }
    }

    private void requestDateSelection() {
        // Get Current Game Date to init picker
        final Calendar c = Calendar.getInstance();
        c.setTime(viewModel.getDraftGame().getDate());
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                (view, year, monthOfYear, dayOfMonth) -> {}, mYear, mMonth, mDay);
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.common_set), (dialog, which) -> {
            c.set(Calendar.YEAR, datePickerDialog.getDatePicker().getYear());
            c.set(Calendar.MONTH, datePickerDialog.getDatePicker().getMonth());
            c.set(Calendar.DAY_OF_MONTH, datePickerDialog.getDatePicker().getDayOfMonth());
            viewModel.getDraftGame().setDate(c.getTime());
            updateDisplay();
            datePickerDialog.dismiss();
        });
        datePickerDialog.show();
    }

    private void requestTimeSelection() {
        // Get Current Game Date to init picker
        final Calendar c = Calendar.getInstance();
        c.setTime(viewModel.getDraftGame().getDate());
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                (view, hourOfDay, minute) -> {
                    c.set(Calendar.HOUR, hourOfDay);
                    c.set(Calendar.MINUTE, minute);
                    viewModel.getDraftGame().setDate(c.getTime());
                    updateDisplay();
                }, mHour, mMinute, true);
        timePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.common_set), (dialog, which) -> {});
        timePickerDialog.show();
    }

}
