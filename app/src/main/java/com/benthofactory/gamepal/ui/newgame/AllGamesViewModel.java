package com.benthofactory.gamepal.ui.newgame;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.Game;
import com.benthofactory.gamepal.model.GamesDao;
import com.benthofactory.gamepal.model.PlayerScore;

import java.util.List;

class AllGamesViewModel extends AndroidViewModel {

    private final GamesDao gamesDao;
    private LiveData<List<Game>> games;

    AllGamesViewModel(Application application) {
        super(application);
        gamesDao = DatabaseHolder.database().gamesDao();
        games = gamesDao.getActiveGamesAsLiveData();
    }

    LiveData<List<Game>> getGames() {
        return games;
    }

    public void deleteGame(Game game) {
        DatabaseHolder.database().gamesDao().deleteGame(game);
    }

    public List<PlayerScore> getPlayerscoresForGame(Game game) {
        return DatabaseHolder.database().gamesDao().getPlayerScoresForGame(game.getId());
    }

}