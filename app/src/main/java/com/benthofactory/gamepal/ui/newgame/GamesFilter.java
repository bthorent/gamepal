package com.benthofactory.gamepal.ui.newgame;

import android.widget.Filter;

import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.Game;
import com.benthofactory.gamepal.model.PlayerScore;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

public class GamesFilter extends Filter {

    GamesRecyclerViewAdapter adapter;
    ArrayList<Game> filterList;

    private int filter_mode = 0;
    public static int FILTER_GAMES = 0;
    public static int FILTER_PLAYERS = 1;

    public GamesFilter(ArrayList<Game> filterList, GamesRecyclerViewAdapter adapter, int filteringMode)
    {
        this.adapter = adapter;
        this.filterList = filterList;
        this.filter_mode = filteringMode;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();

        if(constraint != null && constraint.length() > 0)
        {
            String normalizedConstraint = normalizeString(constraint.toString());

            ArrayList<Game> filteredGames = new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                if (adapter.getFilteringMode() == FILTER_GAMES) {
                    if(normalizeString(filterList.get(i).getName()).contains(normalizedConstraint))
                    {
                        filteredGames.add(filterList.get(i));
                    }
                } else {
                    List<PlayerScore> playersForGame = DatabaseHolder.database().gamesDao().getPlayerScoresForGame(filterList.get(i).getId());
                    boolean found = false;
                    for (PlayerScore ps : playersForGame) {
                        if (normalizeString(ps.getName()).contains(normalizedConstraint)) {
                            found = true;
                        }
                    }
                    if (found) {
                        filteredGames.add(filterList.get(i));
                    }
                }

            }
            results.count = filteredGames.size();
            results.values = filteredGames;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.setGamesList((ArrayList<Game>) results.values);
        adapter.notifyDataSetChanged();
    }

    private static String normalizeString(String stringToNormalize) {
        return Normalizer.normalize(stringToNormalize.toUpperCase(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }
}
