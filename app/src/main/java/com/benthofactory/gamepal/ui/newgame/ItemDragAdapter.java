package com.benthofactory.gamepal.ui.newgame;

public interface ItemDragAdapter {

    void onItemMove(int fromPosition, int toPosition);

    void onItemClear(int fromPosition, int toPosition);

}
