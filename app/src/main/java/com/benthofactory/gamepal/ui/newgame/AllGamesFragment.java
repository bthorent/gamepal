package com.benthofactory.gamepal.ui.newgame;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.Game;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class AllGamesFragment extends Fragment {

    private Menu menu;
    private View mainView;
    private AllGamesViewModel viewModel;
    private OnListFragmentInteractionListener mListener;
    private GamesRecyclerViewAdapter gamesListAdapter;
    private View filterSection;
    private LinearLayout filterBtnsSection;
    private int oldListSize;
    private RecyclerView recyclerView;
    private boolean isFirstLoad = true;
    private SearchView searchView;
    private int SEARCH_MODE = 0;
    private int SEARCH_BY_GAMES = 0;
    private int SEARCH_BY_PLAYERS = 1;

    public AllGamesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void updateDisplay() {
        List<Game> activeGames = DatabaseHolder.database().gamesDao().getAllGames();
        if (activeGames!=null && activeGames.size()>0) {
            mainView.findViewById(R.id.empty_display).setVisibility(View.GONE);
            mainView.findViewById(R.id.games_list_section).setVisibility(View.VISIBLE);
            filterSection.setVisibility(View.VISIBLE);
        } else {
            mainView.findViewById(R.id.empty_display).setVisibility(View.VISIBLE);
            mainView.findViewById(R.id.games_list_section).setVisibility(View.GONE);
            filterSection.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AllGamesViewModelFactory factory = new AllGamesViewModelFactory(requireActivity().getApplication());
        viewModel = new ViewModelProvider(this, factory).get(AllGamesViewModel.class);
        gamesListAdapter = new GamesRecyclerViewAdapter(getContext(), getActivity());
        gamesListAdapter.setViewModel(viewModel);

        gamesListAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                if(!searchView.isIconified()) {
                    if(gamesListAdapter.getItemCount()<1) {
                        mainView.findViewById(R.id.recycler_view).setVisibility(View.GONE);
                        mainView.findViewById(R.id.empty_after_filter).setVisibility(View.VISIBLE);
                    } else {
                        mainView.findViewById(R.id.recycler_view).setVisibility(View.VISIBLE);
                        mainView.findViewById(R.id.empty_after_filter).setVisibility(View.GONE);
                    }
                }
                super.onChanged();
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {return false;}

            @Override
            public boolean onQueryTextChange(String query) {
                gamesListAdapter.setFilteringMode(SEARCH_MODE==SEARCH_BY_GAMES ? GamesFilter.FILTER_GAMES : GamesFilter.FILTER_PLAYERS);
                gamesListAdapter.getFilter().filter(query);
                return false;
            }
        });

        Button searchByGamesButton = mainView.findViewById(R.id.btn_search_by_games);
        Button searchByPlayersButton = mainView.findViewById(R.id.btn_search_by_players);
        searchByGamesButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary, getActivity().getTheme()));
        searchByPlayersButton.setBackgroundColor(getResources().getColor(R.color.grey_20, getActivity().getTheme()));
        searchByGamesButton.setOnClickListener(v -> {
            if (SEARCH_MODE != SEARCH_BY_GAMES) {
                SEARCH_MODE = SEARCH_BY_GAMES;
                searchByGamesButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary, getActivity().getTheme()));
                searchByPlayersButton.setBackgroundColor(getResources().getColor(R.color.grey_20, getActivity().getTheme()));
                gamesListAdapter.setFilteringMode(GamesFilter.FILTER_GAMES);
                String currentQuery = searchView.getQuery().toString();
                searchView.setQuery( currentQuery + " ", false);
                searchView.setQuery( currentQuery, true);
            }
        });

        searchByPlayersButton.setOnClickListener(v -> {
            if (SEARCH_MODE != SEARCH_BY_PLAYERS) {
                SEARCH_MODE = SEARCH_BY_PLAYERS;
                searchByGamesButton.setBackgroundColor(getResources().getColor(R.color.grey_20, getActivity().getTheme()));
                searchByPlayersButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary, getActivity().getTheme()));
                gamesListAdapter.setFilteringMode(GamesFilter.FILTER_PLAYERS);
                String currentQuery = searchView.getQuery().toString();
                searchView.setQuery( currentQuery + " ", false);
                searchView.setQuery( currentQuery, true);
            }
        });

        subscribeUi();
    }

    @Override
    public void onResume() {
        super.onResume();
        isFirstLoad = true;
        subscribeUi();
        updateDisplay();
    }

    private void subscribeUi() {
        viewModel.getGames().observe(getViewLifecycleOwner(), games -> {
            if (games != null) {
                final int size = games.size();

                if (oldListSize != size) {
                    gamesListAdapter.notifyDataSetChanged();
                    /*if (size > 0) {
                        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

                    } else {
                        recyclerView.setLayoutManager(new SpanningLinearLayoutManager(requireContext()));
                    }*/
                }

                gamesListAdapter.setGamesList(new ArrayList<>(games));
                if (size > oldListSize && oldListSize > 0) {
                    recyclerView.smoothScrollToPosition(size);
                }
                if (isFirstLoad) {
                    isFirstLoad = false;
                    recyclerView.post(() -> {
                                recyclerView.setAdapter(gamesListAdapter);
                            }
                    );
                } else {
                    if (oldListSize - 1 == size) {
                        Snackbar.make(recyclerView, getString(R.string.game_deleted), Snackbar.LENGTH_SHORT).show();
                    }
                }
                oldListSize = size;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_games_list, container, false);

        recyclerView = mainView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainView.getContext()));

        filterSection = mainView.findViewById(R.id.games_filter_section);
        filterBtnsSection = mainView.findViewById(R.id.btn_search_mode_section);

        searchView = mainView.findViewById(R.id.gamesSearchView);
        searchView.setOnSearchClickListener(v -> filterBtnsSection.setVisibility(View.VISIBLE));
        searchView.setOnCloseListener(() -> {
            filterBtnsSection.setVisibility(View.GONE);
            return false;
        });

        return mainView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Game item);
    }
}
