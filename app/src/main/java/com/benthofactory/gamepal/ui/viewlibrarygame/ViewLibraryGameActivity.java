package com.benthofactory.gamepal.ui.viewlibrarygame;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;

import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.LibraryGame;
import com.benthofactory.gamepal.ui.editgamelibrary.EditLibraryGameActivity;
import com.github.clans.fab.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import com.benthofactory.gamepal.R;

public class ViewLibraryGameActivity extends AppCompatActivity {

    private LibraryGame gameUnderEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_library_game);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final long id = getIntent().getLongExtra(EditLibraryGameActivity.ARGUMENT_LIBRARY_GAME_ID, 0);
        gameUnderEdit = DatabaseHolder.database().gamesDao().getLibraryGameWithId(id);

        ((TextView) findViewById(R.id.library_game_name)).setText(gameUnderEdit.getName());
        ((TextView) findViewById(R.id.nbrOfPlayersText)).setText(gameUnderEdit.getNumberOfPlayers());
        ((TextView) findViewById(R.id.durationText)).setText(gameUnderEdit.getGameDuration() + " min");
        ((TextView) findViewById(R.id.categoryText)).setText(gameUnderEdit.getCategory());
        int numberOfGamesPlayed = DatabaseHolder.database().gamesDao().getGamesWithName(gameUnderEdit.getName()).size();
        ((TextView) findViewById(R.id.numberOfGamesPlayedText)).setText(numberOfGamesPlayed + " game(s) played");
        ((TextView) findViewById(R.id.gameDescription)).setText(gameUnderEdit.getFreeDescription());
        ((RatingBar) findViewById(R.id.game_rating)).setRating(gameUnderEdit.getRating());

        findViewById(R.id.back_btn).setOnClickListener(v -> finish());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(this, EditLibraryGameActivity.class);
            intent.putExtra(EditLibraryGameActivity.ARGUMENT_LIBRARY_GAME_ID, id);
            startActivity(intent);
            finish();
        });
    }
}
