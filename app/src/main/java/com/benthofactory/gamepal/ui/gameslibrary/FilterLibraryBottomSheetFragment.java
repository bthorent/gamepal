package com.benthofactory.gamepal.ui.gameslibrary;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.LibraryGame;
import com.benthofactory.gamepal.utils.LocalSettings;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import me.bendik.simplerangeview.SimpleRangeView;

public class FilterLibraryBottomSheetFragment extends BottomSheetDialogFragment {

    private DialogInterface.OnDismissListener onDismissListener;

    private int STEP = 5;
    private int minPlayers;
    private int maxPlayers;
    private int minDuration;
    private int maxDuration;

    private EditText gameNameEt;
    private ImageView clearGameNameBtn;
    private SimpleRangeView nbrOfPlayersSeekBar;
    private SimpleRangeView durationSeekBar;
    private TextView nbrPlayersFilterValueText;
    private TextView minPlayerValueText;
    private TextView maxPlayerValueText;
    private TextView durationFilterValueText;
    private TextView minValue;
    private TextView maxValue;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View contentView = inflater.inflate(R.layout.fragment_filter_sheet, container, false);

        gameNameEt = contentView.findViewById(R.id.game_name_et);
        clearGameNameBtn = contentView.findViewById(R.id.clear_game_name_et);

        nbrOfPlayersSeekBar = contentView.findViewById(R.id.nbrOfPlayersSeekBar);
        nbrPlayersFilterValueText = contentView.findViewById(R.id.nbr_players_filter_value);
        minPlayerValueText = contentView.findViewById(R.id.minPlayerValue);
        maxPlayerValueText = contentView.findViewById(R.id.maxPlayerValue);

        durationSeekBar = contentView.findViewById(R.id.minDurationSeekBar);
        durationFilterValueText = contentView.findViewById(R.id.duration_filter_value);
        minValue = contentView.findViewById(R.id.minValue);
        maxValue = contentView.findViewById(R.id.maxValue);

        clearGameNameBtn.setOnClickListener(v -> gameNameEt.setText(""));

        contentView.findViewById(R.id.reset_filters_btn).setOnClickListener(v -> {
            initFilters();
        });

        contentView.findViewById(R.id.confirm_filters_btn).setOnClickListener(v -> {
            saveFilterSelection();
            dismiss();
        });


        computeFiltersLimits();
        //initFilters();
        gameNameEt.setText(LocalSettings.getGameNamePatternFilter());
        setupPlayersFilter();
        setupDurationFilter();


        return contentView;
    }

    private void initFilters() {
        gameNameEt.setText("");
        nbrOfPlayersSeekBar.setStart(0);
        nbrOfPlayersSeekBar.setEnd(maxPlayers-minPlayers);
        durationSeekBar.setStart(0);
        durationSeekBar.setEnd(maxDuration-minDuration);
        updatePlayerCurrentFilterValue();
        updateDurationCurrentFilterValue();
    }

    private void computeFiltersLimits() {
        List<LibraryGame> games = DatabaseHolder.database().gamesDao().getAllGamesInLibrary();
        minPlayers = getMinNumberOfPlayersInGames(games);
        maxPlayers = getMaxNumberOfPlayersInGames(games);
        minDuration = getMinDurationInGames(games);
        maxDuration = getMaxDurationInGames(games);
        minDuration = minDuration/STEP;
        if(maxDuration==(maxDuration/STEP)*STEP) {
            maxDuration = maxDuration/STEP;
        } else {
            maxDuration = maxDuration/STEP + 1;
        }
    }

    private void setupPlayersFilter() {
        minPlayerValueText.setText(String.valueOf(minPlayers));
        maxPlayerValueText.setText(String.valueOf(maxPlayers));
        nbrOfPlayersSeekBar.setCount(maxPlayers-minPlayers+1);
        nbrOfPlayersSeekBar.setMinDistance(0);
        if (LocalSettings.getMinPlayerFilter()!=-1) {
            nbrOfPlayersSeekBar.setStart(LocalSettings.getMinPlayerFilter()-minPlayers);
        } else {
            nbrOfPlayersSeekBar.setStart(0);
        }
        if (LocalSettings.getMaxPlayerFilter()!=-1) {
            nbrOfPlayersSeekBar.setEnd(LocalSettings.getMaxPlayerFilter()-minPlayers);
        } else {
            nbrOfPlayersSeekBar.setEnd(maxPlayers-minPlayers);
        }
        updatePlayerCurrentFilterValue();
        nbrOfPlayersSeekBar.setOnChangeRangeListener((rangeView, start, end) -> updatePlayerCurrentFilterValue());
    }

    private void updatePlayerCurrentFilterValue() {
        if (nbrOfPlayersSeekBar.getStart()==nbrOfPlayersSeekBar.getEnd()) {
            nbrPlayersFilterValueText.setText(String.valueOf(minPlayers+nbrOfPlayersSeekBar.getStart()));
        } else {
            nbrPlayersFilterValueText.setText((minPlayers+nbrOfPlayersSeekBar.getStart()) + "-" + (minPlayers+nbrOfPlayersSeekBar.getEnd()));
        }
    }

    private void setupDurationFilter() {
        minValue.setText(String.valueOf(minDuration*STEP));
        maxValue.setText(String.valueOf(maxDuration*STEP));
        durationSeekBar.setMinDistance(0);
        durationSeekBar.setCount(maxDuration-minDuration+1);
        if (LocalSettings.getMinDurationFilter()!=-1) {
            durationSeekBar.setStart(LocalSettings.getMinDurationFilter()/STEP-minDuration);
        } else {
            durationSeekBar.setStart(0);
        }
        if (LocalSettings.getMaxDurationFilter()!=-1) {
            Log.i("BEN_TEST", "" + LocalSettings.getMaxDurationFilter());
            Log.i("BEN_TEST", "minDuration" + minDuration);
            Log.i("BEN_TEST", "maxDuration" + maxDuration);
            durationSeekBar.setEnd((LocalSettings.getMaxDurationFilter()/STEP)-minDuration);
        } else {
            durationSeekBar.setEnd(maxDuration-minDuration);
        }
        updateDurationCurrentFilterValue();
        durationSeekBar.setOnChangeRangeListener((rangeView, start, end) -> updateDurationCurrentFilterValue());
    }

    private void updateDurationCurrentFilterValue() {
        if (durationSeekBar.getStart()==durationSeekBar.getEnd()) {
            durationFilterValueText.setText((minDuration + durationSeekBar.getStart()) * STEP + " min");
        } else {
            durationFilterValueText.setText(((minDuration+durationSeekBar.getStart())*STEP) + "-" + ((minDuration+durationSeekBar.getEnd())*STEP) + " min");
        }
    }

    private int getMinNumberOfPlayersInGames(List<LibraryGame> games) {
        int minPlayers = 1000;
        for (LibraryGame game : games) {
            if(game.hasNumberOfPlayersRangeValue()) {
                minPlayers = Math.min(minPlayers, Integer.parseInt(game.getMinNumberOfPlayers()));
            } else {
                minPlayers = Math.min(minPlayers, Integer.parseInt(game.getNumberOfPlayers()));
            }
        }
        return  minPlayers;
    }

    private int getMaxNumberOfPlayersInGames(List<LibraryGame> games) {
        int maxPlayers = 1;
        for (LibraryGame game : games) {
            if(game.hasNumberOfPlayersRangeValue()) {
                maxPlayers = Math.max(maxPlayers, Integer.parseInt(game.getMaxNumberOfPlayers()));
            } else {
                maxPlayers = Math.max(maxPlayers, Integer.parseInt(game.getNumberOfPlayers()));
            }
        }
        return  maxPlayers;
    }

    private int getMinDurationInGames(List<LibraryGame> games) {
        int minDuration = 1000;
        for (LibraryGame game : games) {
            if(game.hasDurationRangeValue()) {
                minDuration = Math.min(minDuration, Integer.parseInt(game.getMinDuration()));
            } else {
                minDuration = Math.min(minDuration, Integer.parseInt(game.getGameDuration()));
            }
        }
        return  minDuration;
    }

    private int getMaxDurationInGames(List<LibraryGame> games) {
        int maxDuration = 0;
        for (LibraryGame game : games) {
            if(game.hasDurationRangeValue()) {
                maxDuration = Math.max(maxDuration, Integer.parseInt(game.getMaxDuration()));
            } else {
                maxDuration = Math.max(maxDuration, Integer.parseInt(game.getGameDuration()));
            }
        }
        return  maxDuration;
    }

    private void saveFilterSelection() {
        LocalSettings.saveGameNamePatternFilter(gameNameEt.getText().toString());
        if(nbrOfPlayersSeekBar.getStart()!=0) {
            LocalSettings.saveMinPlayerFilter(nbrOfPlayersSeekBar.getStart()+minPlayers);
        } else {
            LocalSettings.saveMinPlayerFilter(-1);
        }
        if(nbrOfPlayersSeekBar.getEnd()!=maxPlayers-minPlayers) {
            LocalSettings.saveMaxPlayerFilter(nbrOfPlayersSeekBar.getEnd()+minPlayers);
        } else {
            LocalSettings.saveMaxPlayerFilter(-1);
        }
        if(durationSeekBar.getStart()!=0) {
            LocalSettings.saveMinDurationFilter((durationSeekBar.getStart()+minDuration)*STEP);
        } else {
            LocalSettings.saveMinDurationFilter(-1);
        }
        if(durationSeekBar.getEnd()!=maxDuration-minDuration) {
            LocalSettings.saveMaxDurationFilter((durationSeekBar.getEnd()+minDuration)*STEP);
        } else {
            LocalSettings.saveMaxDurationFilter(-1);
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }

    void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        onDismissListener = listener;
    }
}