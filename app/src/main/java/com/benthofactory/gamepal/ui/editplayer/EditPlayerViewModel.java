package com.benthofactory.gamepal.ui.editplayer;

import android.util.SparseIntArray;
import android.util.SparseLongArray;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.benthofactory.gamepal.model.GamesDao;
import com.benthofactory.gamepal.model.PlayerScore;

import java.util.List;

import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

class EditPlayerViewModel extends ViewModel {

    private final GamesDao gamesDao;
    //private final LiveData<List<PlayerScore>> countersLiveData;
    private final LiveData<PlayerScore> counterLiveData;
    private final long id;
    private PlayerScore playerScore;

    EditPlayerViewModel(GamesDao gamesDao, final long counterId) {
        id = counterId;
        this.gamesDao = gamesDao;
        counterLiveData = this.gamesDao.loadCounter(counterId);
        //countersLiveData = gameRepository.getCounters();
    }

    LiveData<PlayerScore> getCounterLiveData() {
        return counterLiveData;
    }

    /*LiveData<List<PlayerScore>> getCounters() {
        return countersLiveData;
    }*/

    public void setPlayerScore(@NonNull PlayerScore c) {
        playerScore = c;
    }

    /*private SparseLongArray buildPositionUpdateUponDelete(@NonNull List<PlayerScore> playerScoreList, PlayerScore deletedPlayer) {

        final SparseLongArray positionMap = new SparseLongArray();
        int deletedPosition = deletedPlayer.getPosition();

        for (int i = 0; i < playerScoreList.size(); i++) {

            int position = playerScoreList.get(i).getPosition();

            if (position > deletedPosition) {
                long id = playerScoreList.get(i).getId();
                int newPosition = position -1;
                positionMap.append(id, newPosition);
            }
        }
        return positionMap;
    }*/


    void deleteCounter() {

        //List<PlayerScore> playerScoreList = countersLiveData.getValue();
        //if (playerScoreList == null) return;

        //final SparseIntArray positionMap = buildPositionUpdateUponDelete(playerScoreList, playerScore);

        /*gameRepository.modifyPositionBatch(positionMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(e -> Timber.e(e, "delete playerScore"))
                .onErrorComplete()
                .subscribe();*/

        gamesDao.deletePlayerScore(playerScore)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void updateColor(@Nullable String hex) {
        /*if (hex == null || hex.equals(playerScore.getColor())) {
            return;
        }*/
        gamesDao.modifyColor(id, hex)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void updateName(@NonNull String newName) {
        /*if (newName.equals(playerScore.getName())) {
            return;
        }*/

        gamesDao.modifyName(id, newName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void updateValue(int value) {
        gamesDao.setScore(id, value)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }
}