package com.benthofactory.gamepal.ui.dice;

import androidx.annotation.IntRange;
import androidx.lifecycle.LiveData;

class DiceLiveData extends LiveData<Integer> {

    private int diceVariant;

    DiceLiveData() {
        setValue(0);
    }

    @Override
    protected void onActive() {
    }

    @Override
    protected void onInactive() {
    }

    void rollDice() {
        setValue(((int) (Math.random() * diceVariant)) + 1);
    }

    public void setDiceVariant(@IntRange(from = 1, to = 100) int diceVariant) {
        this.diceVariant = diceVariant;
    }
}
