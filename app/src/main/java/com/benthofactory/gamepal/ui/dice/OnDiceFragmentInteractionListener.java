package com.benthofactory.gamepal.ui.dice;

public interface OnDiceFragmentInteractionListener {

    void updateCurrentRoll(int number);

}
