package com.benthofactory.gamepal.ui.gameslibrary;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.LibraryGame;
import com.benthofactory.gamepal.ui.gameslibrary.GamesLibraryFragment.OnListFragmentInteractionListener;

import java.util.ArrayList;
import java.util.Collections;

public class LibraryGameRecyclerViewAdapter extends RecyclerView.Adapter<LibraryGameRecyclerViewAdapter.ViewHolder> implements Filterable {

    private ArrayList<LibraryGame> mValues;
    private final OnListFragmentInteractionListener mListener;
    private LibraryGamesFilter filter;
    private OrderingMode orderingMode = OrderingMode.ALPHABETICAL_ORDER;

    enum OrderingMode {
        ALPHABETICAL_ORDER,
        CATEGORY_ORDER,
        RANK_ORDER,
        TIMES_PLAYED_ORDER
    }

    public LibraryGameRecyclerViewAdapter(ArrayList<LibraryGame> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        setOrderingMode(orderingMode);
    }

    public void setGamesList(ArrayList<LibraryGame> items) {
        mValues = items;
        setOrderingMode(orderingMode);
    }

    public void setOrderingMode(OrderingMode mode) {
        orderingMode = mode;
        switch (orderingMode) {
            case ALPHABETICAL_ORDER:
                Collections.sort(mValues, (lhs, rhs) -> lhs.getName().compareTo(rhs.getName()));
                break;
            case CATEGORY_ORDER:
                Collections.sort(mValues, (lhs, rhs) -> {
                    if (lhs.getCategory().equals(rhs.getCategory())) {
                        return lhs.getName().compareTo(rhs.getName());
                    } else {
                        return lhs.getCategory().compareTo(rhs.getCategory());
                    }
                });
                break;
            case RANK_ORDER:
                Collections.sort(mValues, (lhs, rhs) -> {
                    if (lhs.getRating()==rhs.getRating()) {
                        return lhs.getName().compareTo(rhs.getName());
                    } else {
                        return rhs.getRating()-lhs.getRating();
                    }
                });
                break;
            case TIMES_PLAYED_ORDER:
                Collections.sort(mValues, (lhs, rhs) -> {
                    int numberOfTimesLhsGamePlayed = DatabaseHolder.database().gamesDao().getGamesWithName(lhs.getName()).size();
                    int numberOfTimesRhsGamePlayed = DatabaseHolder.database().gamesDao().getGamesWithName(rhs.getName()).size();
                    if (numberOfTimesLhsGamePlayed==numberOfTimesRhsGamePlayed) {
                        return lhs.getName().compareTo(rhs.getName());
                    } else {
                        return numberOfTimesRhsGamePlayed-numberOfTimesLhsGamePlayed;
                    }
                });
                break;
            default:
                Log.i("LibraryGameRecyclerViewAdapter", "Unhandled ordering type");
        }
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new LibraryGamesFilter(mValues,this);
        }
        return filter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_librarygame, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mGameName.setText(mValues.get(position).getName());
        holder.mPlayersDetails.setText(mValues.get(position).getNumberOfPlayers() + " player(s)");
        holder.mDurationDetails.setText(mValues.get(position).getGameDuration() + " min(s)");

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onListFragmentInteraction(holder.mItem);
            }
        });

        holder.mHeaderSection.setVisibility(View.GONE);
        holder.mStarHeaderSection.setVisibility(View.GONE);

        switch (orderingMode) {
            case CATEGORY_ORDER:
                holder.mHeaderSection.setVisibility(View.VISIBLE);
                holder.mHeaderSection.setText(mValues.get(position).getCategory());
                if(position > 0){
                    int i = position - 1;
                    if (i < mValues.size() && mValues.get(position).getCategory().equals(mValues.get(i).getCategory())){
                        holder.mHeaderSection.setVisibility(View.GONE);
                    }
                }
                break;
            case ALPHABETICAL_ORDER:
                holder.mHeaderSection.setVisibility(View.VISIBLE);
                boolean startsWithDigit = mValues.get(position).getName().substring(0,1).matches("[0-9]+");
                holder.mHeaderSection.setText(startsWithDigit ? "#" : mValues.get(position).getName().substring(0,1).toUpperCase());
                if(position > 0){
                    int i = position - 1;
                    if (i < mValues.size() && mValues.get(position).getName().substring(0,1).equals(mValues.get(i).getName().substring(0,1))){
                        holder.mHeaderSection.setVisibility(View.GONE);
                    }
                }
                break;
            case RANK_ORDER:
                holder.mStarHeaderSection.setVisibility(View.VISIBLE);
                holder.mRankTextdetails.setText(String.valueOf(mValues.get(position).getRating()));
                if(position > 0){
                    int i = position - 1;
                    if (i < mValues.size() && mValues.get(position).getRating()==mValues.get(i).getRating()){
                        holder.mStarHeaderSection.setVisibility(View.GONE);
                    }
                }
                break;
            case TIMES_PLAYED_ORDER:
                holder.mHeaderSection.setVisibility(View.VISIBLE);
                int numberOfTimesGamePlayed = DatabaseHolder.database().gamesDao().getGamesWithName(mValues.get(position).getName()).size();
                holder.mHeaderSection.setText(numberOfTimesGamePlayed==0 ? "Never played" : "Played " + numberOfTimesGamePlayed + " time(s)");
                if(position > 0){
                    int i = position - 1;
                    if (i < mValues.size() && DatabaseHolder.database().gamesDao().getGamesWithName(mValues.get(i).getName()).size()==numberOfTimesGamePlayed){
                        holder.mHeaderSection.setVisibility(View.GONE);
                    }
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mGameName;
        public final TextView mPlayersDetails;
        public final TextView mDurationDetails;
        public LibraryGame mItem;
        public View mDivider;
        public final TextView mHeaderSection;
        public View mStarHeaderSection;
        public final TextView mRankTextdetails;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mGameName = (TextView) view.findViewById(R.id.game_name);
            mPlayersDetails = (TextView) view.findViewById(R.id.players_details);
            mDurationDetails = (TextView) view.findViewById(R.id.duration_details);
            mDivider = view.findViewById(R.id.top_border);
            mHeaderSection = (TextView) view.findViewById(R.id.section_info);
            mStarHeaderSection = view.findViewById(R.id.star_section_info);
            mRankTextdetails = (TextView) view.findViewById(R.id.rank_text);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mGameName.getText() + "'";
        }
    }
}
