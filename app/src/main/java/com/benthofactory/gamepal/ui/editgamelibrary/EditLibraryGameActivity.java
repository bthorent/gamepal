package com.benthofactory.gamepal.ui.editgamelibrary;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.LibraryGame;
import com.benthofactory.gamepal.ui.viewlibrarygame.ViewLibraryGameActivity;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Arrays;

public class EditLibraryGameActivity extends AppCompatActivity {

    public static final String ARGUMENT_LIBRARY_GAME_ID = "ARGUMENT_LIBRARY_GAME_ID";

    private LibraryGame gameUnderEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_library_game);

        final long id = getIntent().getLongExtra(ARGUMENT_LIBRARY_GAME_ID, 0);
        if (id != 0) {
            gameUnderEdit = DatabaseHolder.database().gamesDao().getLibraryGameWithId(id);
        }

        MaterialButton createGameButton = findViewById(R.id.save_btn);
        View backButton = findViewById(R.id.back_btn);
        MaterialButton deleteGameButton = findViewById(R.id.delete_btn);
        TextInputEditText gameNameEditText = findViewById(R.id.et_game_name);
        TextInputEditText gameCategoryEditText = findViewById(R.id.et_game_category);
        SwitchMaterial numberPlayersModeSwitch = findViewById(R.id.players_mode_switch);
        TextInputEditText nbrOfPlayersMinValueEditText = findViewById(R.id.et_min_players_value);
        TextInputEditText nbrOfPlayersMaxValueEditText = findViewById(R.id.et_max_players_value);
        TextInputEditText nbrOfPlayersSingleValueEditText = findViewById(R.id.et_game_players_value);
        SwitchMaterial durationModeSwitch = findViewById(R.id.duration_mode_switch);
        TextInputEditText durationMinValueEditText = findViewById(R.id.et_game_duration_range_min);
        TextInputEditText durationMaxValueEditText = findViewById(R.id.et_game_duration_range_max);
        TextInputEditText durationSingleValueEditText = findViewById(R.id.et_game_duration_value);
        TextInputEditText commentsEditText = findViewById(R.id.et_game_comments);
        RatingBar ratingBar = findViewById(R.id.game_rating);

        findViewById(R.id.picture_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (gameUnderEdit!=null) {
            createGameButton.setText("Update");
            gameNameEditText.setText(gameUnderEdit.getName());
            gameCategoryEditText.setText(gameUnderEdit.getCategory());
            if(gameUnderEdit.hasNumberOfPlayersRangeValue()) {
                numberPlayersModeSwitch.setChecked(true);
                findViewById(R.id.number_players_section).setVisibility(View.VISIBLE);
                findViewById(R.id.til_game_players_value).setVisibility(View.GONE);
                nbrOfPlayersMinValueEditText.setText(gameUnderEdit.getMinNumberOfPlayers());
                nbrOfPlayersMaxValueEditText.setText(gameUnderEdit.getMaxNumberOfPlayers());
            } else {
                numberPlayersModeSwitch.setChecked(false);
                findViewById(R.id.number_players_section).setVisibility(View.GONE);
                findViewById(R.id.til_game_players_value).setVisibility(View.VISIBLE);
                nbrOfPlayersSingleValueEditText.setText(gameUnderEdit.getNumberOfPlayers());
            }
            if(gameUnderEdit.hasDurationRangeValue()) {
                durationModeSwitch.setChecked(true);
                findViewById(R.id.game_duration_range_section).setVisibility(View.VISIBLE);
                findViewById(R.id.til_game_duration_value).setVisibility(View.GONE);
                durationMinValueEditText.setText(gameUnderEdit.getMinDuration());
                durationMaxValueEditText.setText(gameUnderEdit.getMaxDuration());
            } else {
                durationModeSwitch.setChecked(false);
                findViewById(R.id.game_duration_range_section).setVisibility(View.GONE);
                findViewById(R.id.til_game_duration_value).setVisibility(View.VISIBLE);
                durationSingleValueEditText.setText(gameUnderEdit.getGameDuration());
            }
            commentsEditText.setText(gameUnderEdit.getFreeDescription());
            ratingBar.setRating(gameUnderEdit.getRating());
        }

        gameCategoryEditText.setOnClickListener(v -> {
            final String[] items = {"Ambiance", "Famille", "Expert"};

            AlertDialog.Builder builder = new AlertDialog.Builder(EditLibraryGameActivity.this);
            builder.setTitle("Select game category");
            builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
            builder.setPositiveButton("Unset", (dialog, which) -> {
                gameCategoryEditText.setText("");
                dialog.dismiss();
            });

            int checkedItem = -1;
            if (gameUnderEdit!=null && gameUnderEdit.getCategory()!=null && !gameUnderEdit.getCategory().isEmpty()) {
                checkedItem = Arrays.asList(items).indexOf(gameUnderEdit.getCategory());
            } else if (!gameCategoryEditText.getText().toString().isEmpty()) {
                checkedItem = Arrays.asList(items).indexOf(gameCategoryEditText.getText().toString());
            }

            builder.setSingleChoiceItems(
                    items,
                    checkedItem,
                    (dialogInterface, i) -> {
                        String selectedItem = Arrays.asList(items).get(i);
                        gameCategoryEditText.setText(selectedItem);
                        dialogInterface.dismiss();
                    });
            builder.show();

        });

        numberPlayersModeSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                findViewById(R.id.number_players_section).setVisibility(View.VISIBLE);
                findViewById(R.id.til_game_players_value).setVisibility(View.GONE);
            } else {
                findViewById(R.id.number_players_section).setVisibility(View.GONE);
                findViewById(R.id.til_game_players_value).setVisibility(View.VISIBLE);
            }
        });

        durationModeSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                findViewById(R.id.game_duration_range_section).setVisibility(View.VISIBLE);
                findViewById(R.id.til_game_duration_value).setVisibility(View.GONE);
            } else {
                findViewById(R.id.game_duration_range_section).setVisibility(View.GONE);
                findViewById(R.id.til_game_duration_value).setVisibility(View.VISIBLE);
            }
        });

        backButton.setOnClickListener(v -> {
            finish();
            if (id!=0) {
                Intent intent = new Intent(this, ViewLibraryGameActivity.class);
                intent.putExtra(EditLibraryGameActivity.ARGUMENT_LIBRARY_GAME_ID, id);
                startActivity(intent);
            }
        });

        createGameButton.setOnClickListener(v -> {
            LibraryGame game = new LibraryGame(gameNameEditText.getText().toString());
            if (gameUnderEdit!=null) {
                game = gameUnderEdit;
                game.setName(gameNameEditText.getText().toString());
            }
            game.setCategory(gameCategoryEditText.getText().toString());
            if(numberPlayersModeSwitch.isChecked()) {
                game.setNumberOfPlayersAsRange(
                        Integer.parseInt(nbrOfPlayersMinValueEditText.getText().toString()),
                        Integer.parseInt(nbrOfPlayersMaxValueEditText.getText().toString())
                );
            } else {
                game.setNumberOfPlayersAsSingleValue(Integer.parseInt(nbrOfPlayersSingleValueEditText.getText().toString()));
            }
            if(durationModeSwitch.isChecked()) {
                game.setGameDurationAsRange(
                        Integer.parseInt(durationMinValueEditText.getText().toString()),
                        Integer.parseInt(durationMaxValueEditText.getText().toString())
                );
            } else {
                game.setGameDurationAsSingleValue(Integer.parseInt(durationSingleValueEditText.getText().toString()));
            }
            game.setFreeDescription(commentsEditText.getText().toString());
            game.setRating((int)ratingBar.getRating());
            DatabaseHolder.database().gamesDao().saveLibraryGame(game);
            if (gameUnderEdit!=null) {
                if (id!=0) {
                    Intent intent = new Intent(this, ViewLibraryGameActivity.class);
                    intent.putExtra(EditLibraryGameActivity.ARGUMENT_LIBRARY_GAME_ID, id);
                    startActivity(intent);
                }
            }
            finish();
        });

        if(gameUnderEdit!=null){
            deleteGameButton.setVisibility(View.VISIBLE);
            deleteGameButton.setOnClickListener(v -> {
                DatabaseHolder.database().gamesDao().deleteLibraryGame(gameUnderEdit);
                finish();
            });
        } else {
            deleteGameButton.setVisibility(View.GONE);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}