package com.benthofactory.gamepal.ui.selectplayers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.utils.LocalSettings;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import static android.content.Context.WINDOW_SERVICE;

public class SelectPlayersFragment extends Fragment {

    private View mView;
    private boolean personMode = true;
    private int numberMode = 1;
    private TextView selectionModeMenuItem;
    private TextView tipRemainingPlayersText;
    private ImageView imageTip;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        // Add Multitouch view for players selection via fingers
        mView = inflater.inflate(R.layout.fragment_select_players, container, false);
        ((RelativeLayout) mView.findViewById(R.id.full_screen)).addView(new MultitouchView(getActivity(), this));

        // Get default selection options in terms of player/group selection and number of those
        personMode = LocalSettings.isPersonDefaultSelectionMode();
        numberMode = LocalSettings.getDefaultPlayersSelectionNumber();

        // Update tip
        tipRemainingPlayersText = mView.findViewById(R.id.tip_selection_remaining_players);
        updateTip(0);

        // Image tip
        imageTip = mView.findViewById(R.id.image_tooltip);
        adaptImageTooltipToPhone();

        // Align local view settings
        LocalSettings.savePlayersSelectionMode(personMode ? "person" : "group");
        LocalSettings.savePlayersSelectionNumber(numberMode);

        // Update mode
        selectionModeMenuItem = mView.findViewById(R.id.selection_mode);
        selectionModeMenuItem.setOnClickListener(v -> showBottomSheet());
        updateModeMenu();

        return mView;
    }

    private void adaptImageTooltipToPhone() {
        imageTip = mView.findViewById(R.id.image_tooltip);
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getContext().getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        int widthInDP = Math.round(dm.widthPixels / dm.density);
        int imageHeightInDP = (int)(imageTip.getLayoutParams().height/dm.density);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)imageTip.getLayoutParams();
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        imageTip.setLayoutParams(params);

        float ratio = (float)((0.8*widthInDP)/imageHeightInDP);
        imageTip.setScaleX(ratio);
        imageTip.setScaleY(ratio);

        imageTip.setTranslationX((ratio-1)*imageHeightInDP);
    }

    private void updateModeMenu() {
        selectionModeMenuItem.setText(numberMode + " " + (personMode ? getString(R.string.menu_mode_person) : getString(R.string.menu_mode_group)));
    }

    @Override
    public void onResume() {
        super.onResume();
        // No keyboard here if present before
        hideKeyboardFrom(getContext(), mView);
    }

    private void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void updateTip(int NumberOfFingersOnScreen) {
        int remainingPlayersNeeded = numberMode - NumberOfFingersOnScreen + 1;
        if (remainingPlayersNeeded > 0 && tipRemainingPlayersText!= null) {
            tipRemainingPlayersText.setText(getResources().getString(R.string.select_player_missing_players, remainingPlayersNeeded));
            tipRemainingPlayersText.setVisibility(View.VISIBLE);
        } else {
            hideTip();
        }
    }

    public void hideTip() {
        if(tipRemainingPlayersText!= null) {
            tipRemainingPlayersText.setVisibility(View.INVISIBLE);
        }
    }

    public void showImageTip() {
        imageTip.setVisibility(View.VISIBLE);
    }

    public void hideImageTip() {
        imageTip.setVisibility(View.GONE);
    }

    private void showBottomSheet() {
        if (this.getFragmentManager() != null) {
            SelectPlayersBottomSheetFragment bottomSheet = new SelectPlayersBottomSheetFragment();
            bottomSheet.show(this.getFragmentManager(), "SelectPlayersBottomSheetFragment");
            bottomSheet.setOnDismissListener(d -> updateOnDismiss());
        } else {
            Snackbar.make(mView, "Unexpected case cannot open bottom sheet", Snackbar.LENGTH_LONG).show();
        }
    }

    private void updateOnDismiss() {
        // Get potentially updated view local settings
        personMode = LocalSettings.isPersonSelectionMode();
        numberMode = LocalSettings.getPlayersSelectionNumber();
        // Update item menu accordingly
        updateModeMenu();
        // Update tip
        updateTip(0);
    }

    public boolean isPersonMode() {
        return personMode;
    }

    public int getNumberMode() {
        return numberMode;
    }

}
