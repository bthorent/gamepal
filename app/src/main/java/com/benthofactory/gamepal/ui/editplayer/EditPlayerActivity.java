package com.benthofactory.gamepal.ui.editplayer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.benthofactory.gamepal.R;
import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.PlayerScore;
import com.benthofactory.gamepal.utils.Utilities;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.thebluealliance.spectrum.SpectrumDialog;


public class EditPlayerActivity extends AppCompatActivity {

    private static final String ARGUMENT_COUNTER_ID = "ARGUMENT_COUNTER_ID";
    private static final String ARGUMENT_COUNTER_COLOR = "ARGUMENT_COUNTER_COLOR";

    private PlayerScore playerScore;
    private TextInputLayout playerScoreNameLayout;
    private AutoCompleteTextView playerScoreName;
    private TextInputEditText pPlayerScoreValue;
    private TextView colorSelection;
    private Button btnSave;
    private EditPlayerViewModel viewModel;
    private String newCounterColorHex;

    public static void start(Activity activity, PlayerScore playerScore) {
        Intent intent = new Intent(activity, EditPlayerActivity.class);
        intent.putExtra(ARGUMENT_COUNTER_ID, playerScore.getId());
        intent.putExtra(ARGUMENT_COUNTER_COLOR, playerScore.getColor());
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_player);

        Bundle extras = getIntent().getExtras();
        if (extras != null && !extras.containsKey(ARGUMENT_COUNTER_ID)) {
            throw new UnsupportedOperationException("Activity should be started using the static start method");
        }

        final long id = getIntent().getLongExtra(ARGUMENT_COUNTER_ID, 0);

        playerScore = DatabaseHolder.database().gamesDao().getPlayerScore(id);

        initViews();

        subscribeToModel(id);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setOnClickListeners();
    }

    private void initViews() {
        playerScoreName = findViewById(R.id.et_counter_name);
        playerScoreName.setText(playerScore.getName());
        /*ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line, DatabaseHolder.database().gamesDao().getAllUniqueActivePlayersNames());
        playerScoreName.setThreshold(1);
        playerScoreName.setAdapter(adapter);*/

        pPlayerScoreValue = findViewById(R.id.et_counter_value);
        pPlayerScoreValue.setText(String.valueOf(playerScore.getScore()));
        playerScoreNameLayout = findViewById(R.id.til_counter_name);
        colorSelection = findViewById(R.id.color_picker);
        int color = Color.parseColor(getIntent().getStringExtra(ARGUMENT_COUNTER_COLOR));
        colorSelection.setBackgroundColor(color);
        newCounterColorHex = String.format("#%06X", 0xFFFFFF & color);
        btnSave = findViewById(R.id.btn_save);

        findViewById(R.id.back_btn).setOnClickListener(v -> finish());
        findViewById(R.id.delete_btn).setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("You're about to delete this player. Are you sure you want to proceed ?")
                    .setPositiveButton(R.string.dialog_yes, (dialog, l1) -> {
                        viewModel.deleteCounter();
                        dialog.dismiss();
                        finish();
                    })
                    .setNegativeButton(R.string.dialog_no, (dialog, l2) -> dialog.dismiss());
            builder.create().show();
        });

    }

    private void setOnClickListeners() {

        colorSelection.setOnClickListener(v -> new SpectrumDialog.Builder(getApplicationContext())
                .setColors(R.array.colors_array)
                .setDismissOnColorSelected(true)
                .setSelectedColor(Color.parseColor(newCounterColorHex))
                .setNegativeButtonText("Cancel")
                .setTitle("Pick a color")
                .setOutlineWidth(1)
                .setOnColorSelectedListener((positiveResult, color) -> {
                    if (positiveResult) {
                        colorSelection.setBackgroundColor(color);
                        newCounterColorHex = String.format("#%06X", 0xFFFFFF & color);
                    }
                }).build().show(getSupportFragmentManager(), "dialog_demo_1"));

        btnSave.setOnClickListener(v -> validateAndSave());
    }

    private void validateAndSave() {
        int newValue = Utilities.parseInt(pPlayerScoreValue.getText().toString());

        viewModel.updateName(playerScoreName.getText().toString());
        viewModel.updateColor(newCounterColorHex);
        viewModel.updateValue(newValue);

        supportFinishAfterTransition();
    }

    private void subscribeToModel(long id) {
        viewModel = new EditPlayerViewModel(DatabaseHolder.database().gamesDao(), id);
        viewModel.getCounterLiveData().observe(this, c -> {
            if (c != null) {
                playerScore = c;
                viewModel.setPlayerScore(c);
                pPlayerScoreValue.setText(String.valueOf(c.getScore()));
                if (!c.getName().equals(playerScoreName.getText().toString())) {
                    playerScoreName.setText(c.getName());
                    playerScoreName.setSelection(c.getName().length());
                }
            } else {
                playerScore = null;
                //finish();
            }
        });
        /*viewModel.getCounters().observe(this, c -> {
        });*/
    }

}