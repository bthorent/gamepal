package com.benthofactory.gamepal.ui.newgame;

import androidx.recyclerview.widget.RecyclerView;
import com.benthofactory.gamepal.model.PlayerScore;

public interface DragItemListener {

    void onStartDrag(RecyclerView.ViewHolder viewHolder);

    void afterDrag(PlayerScore playerScore, int fromIndex, int toIndex);
}
