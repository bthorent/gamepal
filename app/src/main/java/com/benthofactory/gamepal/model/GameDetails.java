package com.benthofactory.gamepal.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class GameDetails {

    @Embedded
    public Game game;

    @Relation(
            parentColumn = "id",
            entityColumn = "gameId",
            entity = PlayerScore.class
    )
    public List<PlayerScore> playerScores;

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public List<PlayerScore> getPlayerScores() {
        return playerScores;
    }

    public void setPlayerScores(List<PlayerScore> playerScores) {
        this.playerScores = playerScores;
    }

    @Override
    public String toString() {
        return "GamesDetails {" +
                "game=" + game +
                ", playerScores=" + playerScores +
                "}";
    }
}
