package com.benthofactory.gamepal.model;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import io.reactivex.Completable;

public class DatabaseHelper {

    private GamesDao gamesDao;

    public DatabaseHelper(DatabaseHolder database) {
        gamesDao = database.gamesDao();
    }

    public void saveGame(Game game) {
        Long id = gamesDao.saveGame(game);
        if (game.getPlayerScores()!=null) {
            savePlayerScores(id, game.getPlayerScores());
        }
    }

    private void insertPlayerScoresForGame(Long id, List<PlayerScore> playerScores) {
        if (id == null) throw new NullPointerException();
        playerScores.forEach(ps -> ps.setGameId(id.intValue()));
    }

    private void savePlayerScores(Long gameId, List<PlayerScore> playerScores) {
        insertPlayerScoresForGame(gameId, playerScores);
        for (PlayerScore ps : playerScores) {
            Long id = gamesDao.savePlayerScore(ps);
        }
    }

    public Completable addRoundToPlayerScore(PlayerScore ps) {
        ps.addRoundsDetail(ps.getCurrentRoundIncrement());
        return gamesDao.setRoundsDetails(ps.getId(), ps.getRoundsDetails());
    }

    public Completable createPlayerScore(long gameId, String name, String color, int position, int numberOfRounds) {
        final PlayerScore playerScore = new PlayerScore(gameId, name, color,position);
        Log.i("BEN_TEST", "Rounds " + numberOfRounds);
        String roundsString = "";
        if (numberOfRounds >= 1){
            roundsString = "0";
        }
        if (numberOfRounds > 1){
            for(int i = 1; i < numberOfRounds; i++) {
                roundsString += ";0";
            }
        }
        playerScore.setRoundsDetails(roundsString);
        return gamesDao.insert(playerScore);
    }

    public Completable modifyScore(long playerScoreId, int difference) {
        return gamesDao.modifyScore(playerScoreId, difference);
    }

    public Completable modifyName(long counterId, String name) {
        return gamesDao.modifyName(counterId, name);
    }

    public Completable setScore(long counterId, int score) {
        return gamesDao.setScore(counterId, score);
    }

    public LiveData<PlayerScore> getPlayerScoresAsLiveData(long playerScoreId) {
        return gamesDao.getPlayerScoresAsLiveData(playerScoreId);
    }

}
