package com.benthofactory.gamepal.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(tableName = "games")
public class Game {

    public static final String GAME_STATUS_DRAFT = "DRAFT";
    public static final String GAME_STATUS_ACTIVE = "ACTIVE";

    @Ignore
    private SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy '-' HH:mm");
    @Ignore
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMMM yyyy");
    @Ignore
    private SimpleDateFormat hourFormatter = new SimpleDateFormat("HH:mm");

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String name;
    private Date date;
    private String status;

    @Ignore
    private List<PlayerScore> playerScores = null;

    public Game() {
        setStatus(Game.GAME_STATUS_DRAFT);
    }

    @Ignore
    public Game(String name, List<PlayerScore> playerScores) {
        this.name = name;
        this.playerScores = playerScores;
        setDate(new Date(System.currentTimeMillis()));
        setStatus(Game.GAME_STATUS_ACTIVE);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public String getDateForDisplay() {
        String displayDate = "Not provided";
        try {
            displayDate = formatter.format(date);
        } catch (Exception e) {}
        return displayDate;
    }

    public String getCalendarDateForDisplay() {
        String displayDate = "Not provided";
        try {
            displayDate = dateFormatter.format(date);
        } catch (Exception e) {}
        return displayDate;
    }

    public String getHourForDisplay() {
        String displayDate = "Not provided";
        try {
            displayDate = hourFormatter.format(date);
        } catch (Exception e) {}
        return displayDate;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isDraftGame() {
        return this.status.equals(GAME_STATUS_DRAFT);
    }

    public List<PlayerScore> getPlayerScores() {
        return playerScores;
    }

    public void setPlayerScores(List<PlayerScore> playerScores) {
        this.playerScores = playerScores;
    }

    public void addPlayerScore(PlayerScore playerScore) {
        if (this.playerScores == null) {
            this.playerScores = new ArrayList<>();
        }
        this.playerScores.add(playerScore);
    }

    public boolean validate() {
        boolean isNameValid = (name != null && !name.isEmpty());
        boolean isPlayerScoresValid = (playerScores != null && playerScores.size()>0);
        return isNameValid && isPlayerScoresValid;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date='" + getDateForDisplay() + '\'' +
                ", status='" + status + '\'' +
                ", playerScores=" + playerScores +
                '}';
    }

}
