package com.benthofactory.gamepal.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(tableName = "librarygames")
public class LibraryGame {

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String name;
    private String category;
    private int rating;
    private String numberOfPlayers;
    private String gameDuration;
    private String freeDescription;
    private String picture;

    @Ignore
    private final String DIVIDER = "-";

    public LibraryGame() {}

    @Ignore
    public LibraryGame(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public String getMinNumberOfPlayers() {
        return numberOfPlayers.split(DIVIDER)[0];
    }

    public String getMaxNumberOfPlayers() {
        return numberOfPlayers.split(DIVIDER)[1];
    }

    public void setNumberOfPlayers(String numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public void setNumberOfPlayersAsRange(int min, int max) {
        this.numberOfPlayers = min + DIVIDER + max;
    }

    public void setNumberOfPlayersAsSingleValue(int value) {
        this.numberOfPlayers = String.valueOf(value);
    }

    public boolean hasNumberOfPlayersRangeValue() {
        return (this.numberOfPlayers!=null && this.numberOfPlayers.contains(DIVIDER));
    }

    public boolean appliesToNbrOfPlayers(int n) {
        if(hasNumberOfPlayersRangeValue()) {
            return (n >= Integer.parseInt(getMinNumberOfPlayers()) && n <= Integer.parseInt(getMaxNumberOfPlayers()));
        }

        return Integer.parseInt(getNumberOfPlayers())==n;
    }

    public String getGameDuration() {
        return gameDuration;
    }

    public String getMinDuration() {
        return gameDuration.split(DIVIDER)[0];
    }

    public String getMaxDuration() {
        return gameDuration.split(DIVIDER)[1];
    }

    public void setGameDuration(String gameDuration) {
        this.gameDuration = gameDuration;
    }

    public void setGameDurationAsRange(int min, int max) {
        this.gameDuration = min + DIVIDER + max;
    }

    public void setGameDurationAsSingleValue(int value) {
        this.gameDuration = String.valueOf(value);
    }

    public boolean hasDurationRangeValue() {
        return (this.gameDuration!=null && this.gameDuration.contains(DIVIDER));
    }

    public boolean appliesToMinDuration(int minDuration) {
        if(hasDurationRangeValue()) {
            return (minDuration <= Integer.parseInt(getMinDuration()));
        }
        return (minDuration <= Integer.parseInt(getGameDuration()));
    }

    public boolean appliesToMaxDuration(int maxDuration) {
        if(hasDurationRangeValue()) {
            return (maxDuration >= Integer.parseInt(getMaxDuration()));
        }
        return (maxDuration >= Integer.parseInt(getGameDuration()));
    }

    public String getFreeDescription() {
        return freeDescription;
    }

    public void setFreeDescription(String freeDescription) {
        this.freeDescription = freeDescription;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "LibraryGame{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", numberOfPlayers='" + numberOfPlayers + '\'' +
                ", gameDuration='" + gameDuration + '\'' +
                ", freeDescription='" + freeDescription + '\'' +
                ", picture='" + picture + '\'' +
                '}';
    }

}
