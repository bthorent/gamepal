package com.benthofactory.gamepal.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "playerscores", foreignKeys = @ForeignKey(entity = Game.class, parentColumns = "id", childColumns = "gameId", onDelete = CASCADE), indices = {@Index("gameId")})
public class PlayerScore {

    @PrimaryKey(autoGenerate = true)
    public long id;

    public String name;
    public int score;
    private String color;
    private String roundsDetails;
    private long gameId;

    private int position;

    public PlayerScore() {
    }

    @Ignore
    public PlayerScore(long gameId, @NonNull String name, String color, int position) {
        this.gameId = gameId;
        this.name = name;
        this.color = color;
        this.position = position;
        score = 0;
        roundsDetails = "";
    }

    @Ignore
    public PlayerScore(String name, int score) {
        super();
        this.name = name;
        this.score = score;
        this.color = "";
        this.roundsDetails = "";
    }

    @Ignore
    public PlayerScore(String name) {
        super();
        this.name = name;
        this.score = 0;
        this.color = "";
        this.roundsDetails = "";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getRoundsDetails() {
        return roundsDetails;
    }

    public int getNumberOfRounds() {
        int numberOfRounds = 0;
        if (roundsDetails != null && !roundsDetails.isEmpty()) {
            numberOfRounds = roundsDetails.split(";").length;
        }
        return numberOfRounds;
    }

    public void setRoundsDetails(String roundsDetails) {
        this.roundsDetails = roundsDetails;
    }

    public int getCurrentRoundsTotal() {
        int total = 0;
        for (String round : this.roundsDetails.split(";")) {
            try {
                total += Integer.parseInt(round);
            } catch (Exception e) {}
        }
        return total;
    }

    public int getCurrentRoundIncrement() {
        return this.score - getCurrentRoundsTotal();
    }

    public void addRoundsDetail(int roundDetail) {
        if (this.roundsDetails==null | this.roundsDetails.isEmpty()) {
            this.roundsDetails = String.valueOf(roundDetail);
        } else {
            this.roundsDetails += ";" + roundDetail;
        }
    }

    public String getRoundsDetailsAsText() {
        String roundsDetailsText = "";
        int index =1;
        for (String roundValue : this.roundsDetails.split(";")) {
            try {
                roundsDetailsText += "#" + index + ": " + (Integer.parseInt(roundValue)>0 ? "+" : "") + roundValue + " |";
                index += 1;
            } catch (Exception e) {}
        }
        if(roundsDetailsText.isEmpty()){
            roundsDetailsText = "Not provided";
        } else {
            roundsDetailsText = roundsDetailsText.substring(0, roundsDetailsText.length() - 2);
        }
        return roundsDetailsText;
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    @Override
    public String toString() {
        return "PlayerScore{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", score='" + score + '\'' +
                ", color='" + color + '\'' +
                ", roundsDetails='" + roundsDetails + '\'' +
                ", gameId=" + gameId +
                '}';
    }
}