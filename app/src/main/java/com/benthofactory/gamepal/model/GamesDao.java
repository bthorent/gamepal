package com.benthofactory.gamepal.model;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.Completable;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public abstract class GamesDao {

    //Game
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract Long saveGame(Game game);


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract void saveGames(List<Game> games);

    //PlayerScore
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract Long savePlayerScore(PlayerScore playerScore);


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract void savePlayerScores(List<PlayerScore> playerScores);


    @Transaction
    @Query("SELECT * FROM games")
    abstract LiveData<List<GameDetails>> loadGames();

    @Transaction
    @Query("SELECT * FROM games")
    abstract List<GameDetails> getGames();

    @Query("SELECT * FROM games WHERE status = 'ACTIVE'")
    public abstract LiveData<List<Game>> getActiveGamesAsLiveData();

    @Query("SELECT * FROM games WHERE status = 'ACTIVE'")
    public abstract List<Game> getAllActiveGames();

    @Query("SELECT * FROM games")
    public abstract LiveData<List<Game>> getAllGamesAsLiveData();

    @Query("SELECT * FROM playerscores WHERE gameId = :iGameId")
    public abstract List<PlayerScore> getPlayerScoresForGame(long iGameId);

    @Query("SELECT * FROM playerscores WHERE gameId = :iGameId")
    public abstract LiveData<List<PlayerScore>> getPlayerScoresForGameAsLiveData(long iGameId);

    @Query("SELECT * FROM games WHERE name = :iName")
    public abstract List<Game> getGamesWithName(String iName);

    @Query("UPDATE playerscores SET score =(score +:difference) WHERE id ==:playerScoreId")
    public abstract Completable modifyScore(long playerScoreId, int difference);

    @Query("UPDATE playerscores " + "SET name = :playerScoreName WHERE id = :playerScoreId")
    public abstract Completable modifyName(long playerScoreId, String playerScoreName);

    @Query("UPDATE playerscores " + "SET score = :score WHERE id = :playerScoreId")
    public abstract Completable setScore(long playerScoreId, int score);

    @Query("select * from playerscores where id = :playerScoreId")
    public abstract LiveData<PlayerScore> getPlayerScoresAsLiveData(long playerScoreId);

    @Query("select * from playerscores where id = :playerScoreId")
    public abstract PlayerScore getPlayerScore(long playerScoreId);

    @Transaction
    public List<Game> getAllGames() {
        List<GameDetails> gameDetails = getGames();
        List<Game> games = new ArrayList<>();
        for (GameDetails gd : gameDetails) {
            Game g = gd.getGame();
            g.setPlayerScores(gd.getPlayerScores());
            games.add(g);
        }
        return games;
    }

    @Query("select * from playerscores where id = :playerScoreId")
    public abstract LiveData<PlayerScore> loadCounter(long playerScoreId);

    @Query("UPDATE playerscores " + "SET color = :hex WHERE id = :playerScoreId")
    public abstract Completable modifyColor(long playerScoreId, String hex);

    @Insert(onConflict = REPLACE)
    public abstract Completable insert(PlayerScore playerScore);

    @Transaction
    public ArrayList<String> getAllUniquePlayerNames() {
        ArrayList<String> names = new ArrayList<>();
        for (PlayerScore ps : getPlayerScores()) {
            names.add(ps.getName());
        }
        return new ArrayList<>( new HashSet<>(names));
    }

    @Transaction
    public ArrayList<String> getAllUniqueGameNames() {
        ArrayList<String> names = new ArrayList<>();
        for (Game g : getAllActiveGames()) {
            names.add(g.getName());
        }
        for (LibraryGame g : getAllGamesInLibrary()) {
            names.add(g.getName());
        }
        return new ArrayList<>( new HashSet<>(names));
    }

    @Transaction
    @Query("SELECT * FROM playerscores")
    public abstract List<PlayerScore> getPlayerScores();

    @Delete
    public abstract Completable deletePlayerScore(PlayerScore playerScore);

    @Query("UPDATE playerscores SET roundsDetails = :roundValue WHERE id =:playerScoreId")
    public abstract Completable setRoundsDetails(long playerScoreId, String roundValue);

    @Delete
    public abstract void deleteGame(Game game);

    @Query("DELETE FROM playerscores")
    public abstract void deleteAllPlayerScores();


    @Query("DELETE FROM games")
    public abstract void deleteAllGames();

    @Query("DELETE FROM librarygames")
    public abstract void deleteAllLibraryGames();

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract Long saveLibraryGame(LibraryGame game);

    @Query("SELECT * FROM librarygames")
    public abstract List<LibraryGame> getAllGamesInLibrary();

    @Query("select * from librarygames where id = :libraryGameId")
    public abstract LibraryGame getLibraryGameWithId(long libraryGameId);

    @Delete
    public abstract void deleteLibraryGame(LibraryGame libraryGame);

}
