package com.benthofactory.gamepal.utils;

import android.content.Context;

public class Singleton {
    private static final Singleton ourInstance = new Singleton();
    private Context mainContext;

    private Singleton() {
    }

    public static Singleton getInstance() {
        return ourInstance;
    }

    public Context getMainContext() {
        return mainContext;
    }

    public void setMainContext(Context mainContext) {
        this.mainContext = mainContext;
    }
}
