package com.benthofactory.gamepal.utils;

import com.benthofactory.gamepal.App;

public class LocalSettings {

    private static final String NAME_IN_APP = "name_in_app";
    private static final String VIBRATE_UPON_SELECTION = "vibrate_upon_selection";
    private static final String DEFAULT_PLAYER_FOR_NEW_GAMES = "default_player_for_new_games";

    private static final String LONG_PRESS_TIP_SHOWED = "long_click_tip_showed";
    private static final String DICE_MAX_SIDE = "dice_max_side";

    private static final String DEFAULT_PLAYERS_SELECTION_MODE = "default_players_selection_mode";
    private static final String PLAYERS_SELECTION_MODE = "players_selection_mode";
    public static final String SELECTION_MODE_PERSON = "person";
    public static final String SELECTION_MODE_GROUP = "group";
    private static final String DEFAULT_PLAYERS_SELECTION_NUMBER = "default_players_selection_number";
    private static final String PLAYERS_SELECTION_NUMBER = "players_selection_number";

    private static final String GAME_NAME_PATTERN_FILTER = "game_name_pattern_filter";
    private static final String MIN_PLAYER_FILTER = "min_player_filter";
    private static final String MAX_PLAYER_FILTER = "max_player_filter";
    private static final String MIN_DURATION_FILTER = "min_duration_filter";
    private static final String MAX_DURATION_FILTER = "max_duration_filter";

    public static void saveGameNamePatternFilter(String gameNamePattern) {
        App.getTinyDB().putString(GAME_NAME_PATTERN_FILTER, gameNamePattern);
    }

    public static String getGameNamePatternFilter() {
        return App.getTinyDB().getString(GAME_NAME_PATTERN_FILTER, "");
    }

    public static int getMinPlayerFilter() {
        return App.getTinyDB().getInt(MIN_PLAYER_FILTER, -1);
    }

    public static void saveMinPlayerFilter(int minPLayerFilter) {
        App.getTinyDB().putInt(MIN_PLAYER_FILTER, minPLayerFilter);
    }

    public static int getMaxPlayerFilter() {
        return App.getTinyDB().getInt(MAX_PLAYER_FILTER, -1);
    }

    public static void saveMaxPlayerFilter(int maxPLayerFilter) {
        App.getTinyDB().putInt(MAX_PLAYER_FILTER, maxPLayerFilter);
    }

    public static int getMinDurationFilter() {
        return App.getTinyDB().getInt(MIN_DURATION_FILTER, -1);
    }

    public static void saveMinDurationFilter(int minDurationFilter) {
        App.getTinyDB().putInt(MIN_DURATION_FILTER, minDurationFilter);
    }

    public static int getMaxDurationFilter() {
        return App.getTinyDB().getInt(MAX_DURATION_FILTER, -1);
    }

    public static void saveMaxDurationFilter(int maxDurationFilter) {
        App.getTinyDB().putInt(MAX_DURATION_FILTER, maxDurationFilter);
    }

    public static int getDiceMaxSide() {
        return App.getTinyDB().getInt(DICE_MAX_SIDE, 6);
    }

    public static void saveDiceMaxSide(int lastSelectedBottomTab) {
        App.getTinyDB().putInt(DICE_MAX_SIDE, lastSelectedBottomTab);
    }

    public static void saveNameInApp(String nameInApp) {
        App.getTinyDB().putString(NAME_IN_APP, nameInApp);
    }

    public static String getNameInApp() {
        return App.getTinyDB().getString(NAME_IN_APP, null);
    }

    public static void saveVibrationUponSelection(boolean activated) {
        App.getTinyDB().putBoolean(VIBRATE_UPON_SELECTION, activated);
    }

    public static boolean isVibrationUponSelectionActivated() {
        return App.getTinyDB().getBoolean(VIBRATE_UPON_SELECTION, true);
    }

    public static void saveDefaultPlayerCreationForNewGames(boolean activated) {
        App.getTinyDB().putBoolean(DEFAULT_PLAYER_FOR_NEW_GAMES, activated);
    }

    public static boolean hasDefaultPlayerCreationForNewGamesActivated() {
        return App.getTinyDB().getBoolean(DEFAULT_PLAYER_FOR_NEW_GAMES, true);
    }

    /*public static boolean getLongPressTipShowed() {
        return App.getTinyDB().getBoolean(LONG_PRESS_TIP_SHOWED);
    }*/

    public static void setLongPressTipShowed() {
        App.getTinyDB().putBoolean(LONG_PRESS_TIP_SHOWED, true);
    }

    public static void saveDefaultPlayersSelectionMode(String selectionMode) {
        App.getTinyDB().putString(DEFAULT_PLAYERS_SELECTION_MODE, selectionMode);
    }

    public static boolean isPersonDefaultSelectionMode() {
        return App.getTinyDB().getString(DEFAULT_PLAYERS_SELECTION_MODE).equals(SELECTION_MODE_PERSON)
                || App.getTinyDB().getString(DEFAULT_PLAYERS_SELECTION_MODE).equals("");
    }

    /*public static boolean isGroupDefaultSelectionMode() {
        return App.getTinyDB().getString(DEFAULT_PLAYERS_SELECTION_MODE).equals(SELECTION_MODE_GROUP)
                && !App.getTinyDB().getString(DEFAULT_PLAYERS_SELECTION_MODE).equals("");
    }*/

    public static void savePlayersSelectionMode(String selectionMode) {
        App.getTinyDB().putString(PLAYERS_SELECTION_MODE, selectionMode);
    }

    public static boolean isPersonSelectionMode() {
        return App.getTinyDB().getString(PLAYERS_SELECTION_MODE).equals(SELECTION_MODE_PERSON)
                || App.getTinyDB().getString(PLAYERS_SELECTION_MODE).equals("");
    }

    /*public static boolean isGroupSelectionMode() {
        return App.getTinyDB().getString(PLAYERS_SELECTION_MODE).equals(SELECTION_MODE_GROUP)
                && !App.getTinyDB().getString(PLAYERS_SELECTION_MODE).equals("");
    }*/

    public static String getGroupSelectionMode() {
        return App.getTinyDB().getString(PLAYERS_SELECTION_MODE, SELECTION_MODE_PERSON);
    }

    public static void savePlayersSelectionNumber(int selectionNumber) {
        App.getTinyDB().putInt(PLAYERS_SELECTION_NUMBER, selectionNumber);
    }

    public static void saveDefaultPlayersSelectionNumber(int selectionNumber) {
        App.getTinyDB().putInt(DEFAULT_PLAYERS_SELECTION_NUMBER, selectionNumber);
    }

    public static int getPlayersSelectionNumber() {
        return App.getTinyDB().getInt(PLAYERS_SELECTION_NUMBER, 1);
    }

    public static int getDefaultPlayersSelectionNumber() {
        return App.getTinyDB().getInt(DEFAULT_PLAYERS_SELECTION_NUMBER, 1);
    }

}
