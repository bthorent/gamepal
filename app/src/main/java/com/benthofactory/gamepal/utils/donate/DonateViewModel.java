package com.benthofactory.gamepal.utils.donate;

import android.app.Activity;
import android.app.Application;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.benthofactory.gamepal.BuildConfig;
import com.benthofactory.gamepal.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.android.billingclient.api.BillingClient.newBuilder;

public class DonateViewModel extends AndroidViewModel implements PurchasesUpdatedListener {

    public final MutableLiveData<SingleShotEvent> eventBus = new MutableLiveData<>();

    private final BillingClient billingClient;

    private final List<SkuDetails> skuDetailsList = new ArrayList<>();
    private final List<String> skuList = BuildConfig.DEBUG
            ? Arrays.asList("android.test.purchased", "android.test.canceled")
            : Arrays.asList("buy_me_a_coffee", "buy_me_a_pizza");

    public DonateViewModel(@NonNull Application application) {
        super(application);
        billingClient = newBuilder(application).setListener(this).enablePendingPurchases().build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                    params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
                    billingClient.querySkuDetailsAsync(params.build(), (result, response) -> {
                        if (result.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            if (response != null) {
                                skuDetailsList.addAll(response);
                            }
                        }
                    });
                    handleUnconsumedPurchases();
                } else {
                    eventBus.postValue(new SingleShotEvent<>(new MessageIntent(R.string.error_message)));
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                billingClient.startConnection(this);
            }
        });
    }

    private void handleUnconsumedPurchases() {
        Purchase.PurchasesResult purchasesResult = billingClient.queryPurchases(BillingClient.SkuType.INAPP);
        List<Purchase> purchases = purchasesResult.getPurchasesList();
        if (purchases != null && !purchases.isEmpty()) {
            consumePurchases(purchases, null);
        }
    }

    void purchase(Activity activity, @IntRange(from = 0, to = 1) int donateOption) {
        String sku = skuList.get(donateOption);
        SkuDetails skuDetails = findSkuDetails(sku);
        if (skuDetails == null) {
            return;
        }
        billingClient.launchBillingFlow(activity, BillingFlowParams.newBuilder()
                .setSkuDetails(skuDetails)
                .build());
    }

    private SkuDetails findSkuDetails(@NonNull String sku) {
        for (SkuDetails skuDetails : skuDetailsList) {
            if (sku.equals(skuDetails.getSku())) {
                return skuDetails;
            }
        }
        return null;
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && purchases != null && !purchases.isEmpty()) {
            consumePurchases(
                    purchases,
                    () -> {
                        eventBus.postValue(new SingleShotEvent<>(new CloseScreenIntent(R.string.donation_thank_you)));
                    }
            );
        } else {
            if (billingResult.getResponseCode() != BillingClient.BillingResponseCode.USER_CANCELED) {
                eventBus.postValue(new SingleShotEvent<>(new MessageIntent(R.string.error_message)));
            }
        }
    }

    private void consumePurchases(@NonNull List<Purchase> purchases, @Nullable Runnable onCompleted) {
        if (purchases.isEmpty()) {
            if (onCompleted != null) onCompleted.run();
            return;
        }

        final AtomicInteger countDown = onCompleted == null ? null : new AtomicInteger(purchases.size());

        for (Purchase purchase : purchases) {
            ConsumeParams consumeParams = ConsumeParams.newBuilder()
                    .setPurchaseToken(purchase.getPurchaseToken())
                    .build();
            billingClient.consumeAsync(
                    consumeParams,
                    (result, token) -> {
                        if (onCompleted != null && countDown.decrementAndGet() <= 0) {
                            onCompleted.run();
                        }
                    }
            );
        }
    }

    @Override
    protected void onCleared() {
        billingClient.endConnection();
    }

    private static class BillingStateException extends RuntimeException {
        BillingStateException(String message) {
            super(message);
        }
    }
}
