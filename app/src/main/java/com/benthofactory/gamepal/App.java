package com.benthofactory.gamepal;

import android.app.Application;

import com.benthofactory.gamepal.utils.Singleton;
import com.benthofactory.gamepal.utils.TinyDB;


public class App extends Application {

    private static TinyDB tinyDB;
    private static App instance;

    public static TinyDB getTinyDB() {
        if (tinyDB == null) {
            tinyDB = new TinyDB(Singleton.getInstance().getMainContext());
        }
        return tinyDB;
    }

    public static App getInstance() {
        return instance;
    }

}
