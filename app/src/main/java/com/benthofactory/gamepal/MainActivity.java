package com.benthofactory.gamepal;

import android.content.Intent;
import android.os.Bundle;

import com.benthofactory.gamepal.model.DatabaseHolder;
import com.benthofactory.gamepal.model.Game;
import com.benthofactory.gamepal.model.LibraryGame;
import com.benthofactory.gamepal.ui.editgamelibrary.EditLibraryGameActivity;
import com.benthofactory.gamepal.ui.newgame.AllGamesFragment;
import com.benthofactory.gamepal.ui.gameslibrary.GamesLibraryFragment;
import com.benthofactory.gamepal.ui.dice.OnDiceFragmentInteractionListener;
import com.benthofactory.gamepal.ui.viewlibrarygame.ViewLibraryGameActivity;
import com.benthofactory.gamepal.utils.Singleton;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity  implements AllGamesFragment.OnListFragmentInteractionListener, GamesLibraryFragment.OnListFragmentInteractionListener, OnDiceFragmentInteractionListener {

    private int currentDiceRoll;
    private int previousDiceRoll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);
        Singleton.getInstance().setMainContext(this);

        DatabaseHolder.init(this);
    }

    @Override
    public void updateCurrentRoll(int number) {
        previousDiceRoll = currentDiceRoll;
        currentDiceRoll = number;
    }

    @Override
    public void onListFragmentInteraction(Game item) {}

    @Override
    public void onListFragmentInteraction(LibraryGame item) {
        Intent intent = new Intent(this, ViewLibraryGameActivity.class);
        intent.putExtra(EditLibraryGameActivity.ARGUMENT_LIBRARY_GAME_ID, item.getId());
        startActivity(intent);
    }

}
