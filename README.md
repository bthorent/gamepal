# GamePal 

### What is it ?
GamePal is an android application for boardgame addicts that provides several utils. 

### Features
* **Game handling** A way to record games you had. Handles rounds if needed. Once saved you can access all your past games and have statistics on it.
* **Game library** Store all your games with basic information on it (name, number of player, duration, details, times played). It also embeds a search mechanism to easily select a game to play acoording to main criteria of duration and number of players.
* **Tool for players selection** Select mode (number of players to be selected, number of groups to create) then ask all players to put their fingers on screen and let the app do the rest and randomly perform selection requested.
* **Dice** Select the kind of dice you want to roll. Touch screen or move your phone to roll !
* **Settings** Most of the features are configurable, all available in this panel !

### Installing
Not yet available on Android store but you can get it here and install it immediately in developer mode.


### Contributing
Interested in participating in this project ? The more the merrier ! Don't hesitate to contact me !

### Author
**Benjamin Thorent**

### License
This project is licensed under the MIT License.

